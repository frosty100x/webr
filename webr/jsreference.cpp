
#include "engine.h"
#include "const.h"
#include "jsreference.h"
#include "jsstring.h"

namespace webr{
	using namespace Internal::Enumerations;
	JSReference::JSReference(std::string name, JSValue *value) : Node(NodeType::REFERENCE){
		ref_object = value;
		ref_name = name;
	}

	JSReference::JSReference(std::string name) : Node(NodeType::REFERENCE){
		ref_object = nullptr;
		ref_name = name;
	}

	JSValue* JSReference::Value(){
		return ref_object;
	}

	std::string JSReference::Name(){
		return ref_name;
	}

	void JSReference::Set(JSValue *v){
		ref_object = v;
	}

	JSValue* JSReference::getValue(Engine *e, Routine *owner){
		return (JSValue*)e->Undefined();
	}


}
