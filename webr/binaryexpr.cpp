
#include "parser.h"
#include "engine.h"
#include "jsvalue.h"
#include "jsnumber.h"
#include "jsstring.h"
#include "jsundefined.h"
#include "jsbool.h"
#include "routine.h"
#include "errortypes.h"
#include <iostream>
#include <math.h>
namespace webr {

	JSBinaryExpr::~JSBinaryExpr(){}
	JSBinaryExpr::JSBinaryExpr(std::deque<Node*> _nodes)
										: Expression(ExpressionType::BINARY), nodes(_nodes) {
	}

	JSValue* JSBinaryExpr::getValue(Engine *e, Routine *owner){
		this->e = e;
		std::vector<Node*> stack;
		/*
		for(size_t n=0; n<nodes.size(); n++){
			Node *node = nodes.at(n);
			if(node->IsAPunctuator())
				std::cout << Punctuators::From(node->ToAPunctuator()->getPunc());
			else
				std::cout << node->getValue(e, owner)->ToStringValue();
		}

		return e->NewString("\n");
	*/
		//for(size_t i=nodes.size()-1; i>0; i--){
		for(size_t i=0; i<nodes.size(); i++){
			Node *n = nodes.at(i);

			if(n->IsAPunctuator()){
				if(stack.empty()){
					throw SyntaxError("");
				}

				else if(stack.size() < 2){

					if(!IsUnary(n->ToAPunctuator()->getPunc()))
						throw SyntaxError("");

					Node * value = stack.back(); stack.pop_back();
					stack.push_back(UnaryEval(n->ToAPunctuator()->getPunc(), value->ToJSValue()));
				}else {
					Node * rhs = stack.back(); stack.pop_back();
					Node * lhs = stack.back(); stack.pop_back();
					stack.push_back(Eval(n->ToAPunctuator()->getPunc(), lhs->ToJSValue(), rhs->ToJSValue()));
					owner->holt();
				}
			}else{
				stack.push_back(n->getValue(e, owner));
			}
		}
		return (stack.size() == 1) ? stack.back()->ToJSValue() : NULL;
	}


	JSValue* JSBinaryExpr::UnaryEval(int op, JSValue *v){
		if(!v->IsNumber())
			return e->NaN();

		switch(op){
		case Punctuators::PLUS:
			return e->NewNumber(+ v->ToNumber()->Value());

		case Punctuators::MINUS:
			return e->NewNumber(- v->ToNumber()->Value());
		}

		return e->NaN();
	}

	JSValue* JSBinaryExpr::Eval(int op, JSValue *lhs, JSValue *rhs){
		switch(op){
			case Punctuators::PLUS: return Add(lhs, rhs);
			case Punctuators::MINUS: return Subtract(lhs, rhs);
			case Punctuators::MULTIPLY: return Multiply(lhs, rhs);
			case Punctuators::DIVIDE: return Divide(lhs, rhs);
			case Punctuators::EQUAL_TO: return Equal_To(lhs, rhs)->ToJSValue();
			case Punctuators::GREATER_THAN: return Greater_Than(lhs, rhs)->ToJSValue();
			case Punctuators::LESS_THAN: return Less_Than(lhs, rhs);
			case Punctuators::REMAINDER: return Remainder(lhs, rhs);

			default: return NULL;
		}

	}

	JSValue* JSBinaryExpr::Less_Than(JSValue *lhs, JSValue *rhs){
		if(lhs->ValueType() == rhs->ValueType()){

			if(lhs->IsNumber()){
				JSNumber &rlhs = *lhs->ToNumber(), &rrhs = *rhs->ToNumber();
				if(rlhs.IsNaN() || rrhs.IsNaN()) return e->False();
				return e->NewBoolean(rlhs.Value() < rrhs.Value());
			}

		}
		return e->False();
	}

	JSBool* JSBinaryExpr::Greater_Than(JSValue *lhs, JSValue *rhs){
		if(lhs->ValueType() == rhs->ValueType()){

			if(lhs->IsNumber()){
				JSNumber &rlhs = *lhs->ToNumber(), &rrhs = *rhs->ToNumber();
				if(rlhs.IsNaN() || rrhs.IsNaN()) return e->False();
				return e->NewBoolean(rlhs.Value() > rrhs.Value());
			}

		}
		return e->False();
	}

	JSBool* JSBinaryExpr::Equal_To(JSValue *lhs, JSValue *rhs){
		if(lhs->ValueType() == rhs->ValueType()){
			if(lhs->IsUndefined()) return e->True();

			//TODO: if lhs is null return true

			if(lhs->IsNumber()){
				JSNumber &rlhs = *lhs->ToNumber(), &rrhs = *rhs->ToNumber();

				if(rlhs.IsNaN() || rrhs.IsNaN()) return e->False();
				else if(rlhs.Value() == rrhs.Value()) return e->True();

				//TODO: if +0 == -0 return true and vis versa
				else return e->False();

			}else if(lhs->IsString()){
				if(lhs->ToStringValue() == rhs->ToStringValue()) return e->True();
				else return e->False();
			}else if(lhs->IsBoolean()){
				return e->NewBoolean( lhs->ToBooleanValue() == rhs->ToBooleanValue() );
			}else if(lhs->IsObject()){
				return e->NewBoolean(lhs == rhs);
				//TODO:  if lhs refers to same object as rhs return true
			}
		}else{
			//if null and undefined return true and vis versa
			if(lhs->IsNumber() && rhs->IsString()){

			}
			else if(rhs->IsNumber() && lhs->IsString()){

			}

		}

		return e->False();
	}

	JSValue* JSBinaryExpr::Remainder(JSValue *lhs, JSValue *rhs){
		if((lhs->IsNumber() && rhs->IsNumber()) && !(lhs->ToNumber()->IsNaN() || rhs->ToNumber()->IsNaN()))
			//return e->NewNumber(remainder(lhs->ToNumber()->Value() rhs->ToNumber()->Value()));
			return e->NewNumber(remainder(lhs->ToNumber()->Value(), rhs->ToNumber()->Value()));
		else
			return e->NaN();

	}

	JSValue* JSBinaryExpr::Divide(JSValue *lhs, JSValue *rhs){
		if(lhs->IsNumber() && rhs->IsNumber() && !(lhs->ToNumber()->IsNaN() || rhs->ToNumber()->IsNaN())){
			return e->NewNumber(lhs->ToNumber()->Value() / rhs->ToNumber()->Value());
		}else{
			return e->NaN();
		}
	}

	JSValue* JSBinaryExpr::Multiply(JSValue *lhs, JSValue *rhs){
		if(lhs->IsNumber() && rhs->IsNumber() && !(lhs->ToNumber()->IsNaN() || rhs->ToNumber()->IsNaN())){
			return e->NewNumber(lhs->ToNumber()->Value() * rhs->ToNumber()->Value());
		}else{
			return e->NaN();
		}
	}

	JSValue* JSBinaryExpr::Subtract(JSValue *lhs, JSValue *rhs){
		if(lhs->IsNumber() && rhs->IsNumber() && !(lhs->ToNumber()->IsNaN() || rhs->ToNumber()->IsNaN())){
			return e->NewNumber(lhs->ToNumber()->Value() - rhs->ToNumber()->Value());
		}else{
			return e->NaN();
		}
	}

	JSValue* JSBinaryExpr::Add(JSValue* lhs, JSValue *rhs){

		if(lhs->IsNumber() && rhs->IsNumber()){
			return e->NewNumber(lhs->ToNumber()->Value() + rhs->ToNumber()->Value());
		}else{
			return e->NewString(lhs->ToStringValue() + rhs->ToStringValue())->ToJSValue();
		}
		return NULL;
	}

	bool JSBinaryExpr::AnyString(JSValue *a, JSValue *b){
		return (a->IsString() || b->IsString());
	}

	bool JSBinaryExpr::IsUnary(int punc){
		switch(punc){
		case Punctuators::PLUS: case Punctuators::MINUS: case Punctuators::NOT:
			return true;
		default: return false;
		}
	}

	int JSBinaryExpr::Type(){
		return ExpressionType::BINARY;
	}

	int JSBinaryExpr::BaseType(){
		return 0;
	}

}
