#ifndef WEBR_XJSSTRING
#define WEBR_XJSSTRING

#include "stddef.h"
#include "sys/types.h"
#include "huballocator.h"
#include "xclass.h"
#include "const.h"
namespace webr {

	namespace Internal {
		namespace JSObjects {
			namespace xJSString {

				namespace {

				}

				intptr_t make(char* str) {
					char *ptr = (char*)HubAllocator::getSpace(strlen(str) + 1);
					ptr[OBJECT_TYPE_OFFSET] = (char)Enumerations::LinkedType::JSSTRING;
					(*(char*)(ptr + OBJECT_VALUE_OFFSET)) = *str;
					return (intptr_t)ptr;
				}

				intptr_t make(int size) {
					char *ptr = (char*)HubAllocator::getSpace(size);
					ptr[OBJECT_TYPE_OFFSET] = (char)Enumerations::LinkedType::JSSTRING;
					(*(char*)(ptr + OBJECT_VALUE_OFFSET)) = '\0';
					return (intptr_t)ptr;
				}
				void setValue(intptr_t objectPtr, char* value) {

				}
				//char* getValue(intptr_t objectPtr){
				//
				//
				//
				//}
			}
		}
	}
}
#endif //WEBR_XJSSTRING
