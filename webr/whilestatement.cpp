#include "parser.h"
#include "engine.h"
#include "jsundefined.h"
#include "routine.h"
#include <iostream>
namespace webr {

	While::While(Node *_control, Node *_block) : Node(NodeType::WHILE_STATEMENT){
		control = _control;
		block = _block;
	}

	JSValue* While::getValue(Engine *e, Routine *owner){

		for(;;){

			bool enter = control->getValue(e, owner)->ToBooleanValue();
			if(enter){
				if(block != NULL) block->getValue(e, owner);
				//std::cout << "enter;";
			}
			else break;
			owner->holt();
		}
		return e->Undefined();
	}
}
