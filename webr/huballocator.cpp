#include "huballocator.h"

#include <stdio.h>
#include "const.h"


#ifdef _WIN32
#include <Windows.h>	
//#include <windef.h>
	//#include <WinBase.h>
#elif _LINUX
	#include <sys/mmap.h>
#endif

#pragma warning

namespace webr {
	namespace Internal {
		namespace HubAllocator {
			namespace {
				void format(char* page, int32_t size) {
					page[0] = (char)Enumerations::LinkedType::DATA_PAGE;
					((int32_t*)(page + PAGE_SIZE_OFFSET))[0] = INIT_PAGE_SIZE;
				}

				void* makeSpace() {
#ifdef _LINIX
					void *mem = mmap(NULL, INIT_PAGE_SIZE, PROT_READ | PROT_WRITE,
						MAP_ANON | MAP_PRIVATE, -1, 0);
					if (mem == MAP_FAILED)
#elif _WIN32
					void *mem = VirtualAlloc(NULL, INIT_PAGE_SIZE, MEM_COMMIT, PAGE_READWRITE);
					if (mem == NULL)
#endif
						throw "ERROR";
					else {
						format((char*)mem, INIT_PAGE_SIZE);
						avalible = INIT_PAGE_SIZE;
					}
					return mem;
				}
			}

			void* getSpace(size_t size) {
				if (size >= avalible) {
					pageHead = (char*)makeSpace();
					spaceHead = pageHead + PAGE_SPACE_OFFSET;
					avalible -= size;
				}
				else {
					spaceHead += _lastAllocation;
				}

				_lastAllocation = size;
				return spaceHead;
			}

			void* getExecutableSpace(size_t size) {
				return makeSpace();
			}
		}
	}
}
