#include <iostream>
#include <deque>
#include <stack>
#include "node.h"
#include "parser.h"
#include "const.h"
#include "jsvalue.h"
#include "jsnumber.h"
#include "jsstring.h"
#include "jsobject.h"
#include "jsbool.h"
#include "jsreference.h"
#include "errortypes.h"
#include "jsundefined.h"
using namespace std;
namespace webr {
	Parser::Parser(){
		this->pos = 0;
		currentSlot = 0;
	}

	Parser::Parser(std::vector<Token> _tokens){
		this->tokens = _tokens;
		this->pos = -1;
		this->InAssign = false;
		this->InBinOp = false;
		this->InFuncCall = false;
	}

	void Parser::operator()(std::vector<Token> _tokens){
		this->tokens = _tokens;
		this->pos = -1;
	}

	std::vector<Node*>& Parser::parse(){
		Token to = nextNonNewLine();
		Node *n = nullptr;

		do{
			if(current().punctuator == Punctuators::STATMENT_TERMINATOR){
				nextSkipSemicolon(); continue;
			}
			if(current().type == TokenType::NONE) break;
			n = handle();
			if(n != NULL) ast.push_back(n);
		}while(n != NULL);
		return ast;
	}

	Node* Parser::handle(){
		Token to = current();

		if(to.type == TokenType::EOF_EOF || to.punctuator == Punctuators::STATMENT_TERMINATOR)
			return NULL;

		if(Node *n = handleBlock()) return n;

		if(Node *n = handleFork())
			return n;

		if(Node *n = handleIfSt())
			return n;

		if(Node *n = handleWhileST())
			return n;

		if(Node *n = handleExpression(Punctuators::STATMENT_TERMINATOR, false))
			return n;

		unexpected(current());
	}

	Node* Parser::handleExpression(int terminator, bool inBinOperation){
		Token to = current();

		Node *lastExpr = nullptr;
		if(to.punctuator == Punctuators::STATMENT_TERMINATOR){
			//nextNonNewLine();
			return NULL;
		}

		else if(to.punctuator == terminator){
		//	nextNonNewLine();
			return NULL;
		}

		else if((lastExpr = handleObjectLit())){}

		else if((lastExpr = handleWord())){}

		else if((lastExpr = handleTerm())){}


		bool result = false;
		while((result = handleSecondaryExpr(lastExpr, terminator)));

		if(!result)
			return lastExpr;

		/*
		if(lastExpr != NULL){
			to = current();

			if(to.punctuator == Punctuators::OPEN_PARENTHESES){
				Node *tmp = handleFunctionCall(terminator, lastExpr);
				lastExpr = (tmp != NULL) ? tmp : lastExpr;
			}else if(!InAssign && to.punctuator == Punctuators::ASSIGN){
				Node *tmp = handleAssignment(Punctuators::ADD_ASSIGN, false, lastExpr);
				 lastExpr = (tmp != NULL) ? tmp : lastExpr;
			}else if(to.punctuator == Punctuators::STATMENT_TERMINATOR){
				return lastExpr;
			}else if( to.punctuator == terminator){
				return lastExpr;
			}else if(to.punctuator == Punctuators::PROPERTY_ACCESSOR){
				Node *tmp = handleModifier(lastExpr);
				lastExpr = (tmp != NULL) ? tmp : lastExpr;
			}

			to = current();

			if(IsBinOperant(to))
				return inBinOperation ? lastExpr : handleBinaryExpr(terminator, lastExpr);

			if(!isStopper(to)){
				if(back(1).type != TokenType::LINE_TERMINATOR)
					throw SyntaxError("Unexpected token: ", to);
			}
			return lastExpr;
		}
		*/

		if(IsBinOperant(to))
			return handleBinaryExpr(terminator, NULL);

		if(to.punctuator == Punctuators::OPEN_PARENTHESES)
			return handleBinaryExpr(terminator, NULL);

		return NULL;
	}

	bool Parser::handleSecondaryExpr(Node *&lastExpr, int terminator){
		Token to = current();

		if(to.punctuator == Punctuators::OPEN_PARENTHESES){
			Node* t = lastExpr;
			Node *tmp = handleFunctionCall(t);
			lastExpr = (tmp != NULL) ? tmp : lastExpr;
		}else if(!InAssign && to.punctuator == Punctuators::ASSIGN){
			Node *tmp = handleAssignment(false, lastExpr);
			 lastExpr = (tmp != NULL) ? tmp : lastExpr;
		}else if(to.punctuator == Punctuators::STATMENT_TERMINATOR){
			/* nextNonNewLine(); */ return false;
		}/*else if( to.punctuator == terminator){
			return false;
		}*/else if(to.punctuator == Punctuators::PROPERTY_ACCESSOR){
			Node *tmp = handleModifier(lastExpr);
			lastExpr = (tmp != NULL) ? tmp : lastExpr;
			return true;
		}else if(IsBinOperant(to) && !InBinOp){
			lastExpr = handleBinaryExpr(terminator, lastExpr);
			return true;
		}

		//to = current();

		//if(!isStopper(to)){
			//if(back(1).type != TokenType::LINE_TERMINATOR)
				//throw SyntaxError("Unexpected token: ", to);
		//}
		return false;
	}

	Node* Parser::handleModifier(Node *firstNode){
		Token to = current();

		bool expectPeriod = true;
		ModifierExpr *modeExpr = new ModifierExpr(firstNode);

		while(to.type != TokenType::EOF_EOF){
			if(expectPeriod && to.punctuator != Punctuators::PROPERTY_ACCESSOR){
				if(to.punctuator == Punctuators::STATMENT_TERMINATOR)
					nextSkipSemicolon();
				break;
			}
			else{
				expectPeriod = false;
				to = nextNonNewLine();
			}

			//to = nextNonNewLine();

			if(to.type == TokenType::IDENTIFIER){
				modeExpr->addMemberAccess(to.literalValue);
				expectPeriod = true;
			}
			else unexpected(to);

			to = nextNonNewLine();
		}
		return modeExpr;
	}

	Node* Parser::handleFork(){
		if(current().word != KeyWord::ROUTINE) return NULL;

		nextNonNewLine();
		Node *exp = handle();

		if(exp == NULL)
			unexpected(current());

		return new ForkExpr(exp);
	}

	Node* Parser::handleIfSt(){
		if(current().word != KeyWord::IF) return NULL;

		std::vector<Node*> controls;
		std::vector<Node*> blocks;
		Node *final_else = nullptr;
		bool iniIf = false;
		bool lastElse = false;

		do{
			//nextNonNewLine();
			if(current().word == KeyWord::IF){
				if(iniIf) break;
					//throw SyntaxError("Unexpected token: ", current());
				iniIf = true;
				nextNonNewLine();
			}else if(current().word == KeyWord::ELSE){
				nextNonNewLine();

				if(current().word == KeyWord::IF){
					nextNonNewLine();
				}else
					lastElse = true;

			}else if(current().punctuator == Punctuators::STATMENT_TERMINATOR){
				nextSkipSemicolon(); continue;
			}else
				break;

			if(!lastElse){
				if(current().punctuator != Punctuators::OPEN_PARENTHESES)
					throw SyntaxError("Unexpected token: ", current());
				nextNonNewLine();

				Node *exp = handleExpression(Punctuators::CLOSE_PERENTHESES, false);

				if(exp == NULL)
					break;
				controls.push_back(exp);

				if(current().punctuator != Punctuators::CLOSE_PERENTHESES)
					throw SyntaxError("Unexpected token: ", current());
				nextNonNewLine();
			}

			Node *block = handle();
			if(block == NULL)
				break;

			if(lastElse){
				final_else = block; break;
			}
			else
				blocks.push_back(block);

		}while(current().type != TokenType::EOF_EOF);

		return new IfStatement(controls, blocks, final_else);
	}

	Node* Parser::handleWhileST(){
		Token to = current();

		if(to.word == KeyWord::WHILE) nextNonNewLine();
		else return NULL;

		if(current().punctuator != Punctuators::OPEN_PARENTHESES)
			throw SyntaxError("Unexpected token: ", current());
		nextNonNewLine();

		Node *control = handleExpression(Punctuators::CLOSE_PERENTHESES, false);
		if(current().punctuator != Punctuators::CLOSE_PERENTHESES || control == NULL)
			throw SyntaxError("Unexpected token: ", current());
		else
			nextNonNewLine();

		if(current().punctuator == Punctuators::STATMENT_TERMINATOR)
			return new While(control, NULL);

		Node *block = handle();

		return new While(control, block);
	}

	Node* Parser::handleBlock(){
		if(!(current().punctuator == Punctuators::BlOCK_START && !(InAssign || InFuncCall)))
			return NULL;

		Token to = current();
		vector<Node*> exps;

		nextNonNewLine();

		Token s = current();
		while(current().punctuator != Punctuators::BLOCK_END && current().type != TokenType::EOF_EOF){
			if(current().punctuator == Punctuators::STATMENT_TERMINATOR){
				nextNonNewLine(); continue;
			}
			Node *exp = handle();
			if(exp != NULL)
				exps.push_back(exp);
			else break;
		}

		if(current().punctuator == Punctuators::STATMENT_TERMINATOR)
			nextNonNewLine();

		if(current().punctuator != Punctuators::BLOCK_END)
			throw SyntaxError("Unexpected token: ", current());

		nextNonNewLine();
		return new Block(exps);
	}



	Node* Parser::handleAssignment(bool isVar, Node *firstNode){
		if(isVar && (InFuncCall || InBinOp)) return NULL;
		InAssign = true;
		Token to = current();
		vector<vector<Node*>> asnList;
		int asnDepth = 0;
		bool idenFound = false;
		asnList.push_back(vector<Node*>());

		if(!isVar){
			idenFound = true;
			asnList.at(0).push_back(firstNode);
		}else
			nextNonNewLine();


		while(current().type != TokenType::NONE){
			if(!idenFound){
				idenFound = true;
				if(current().type == TokenType::IDENTIFIER){
					asnList.at(asnDepth).push_back(new IdentifierExpr(current().literalValue));
					nextNonNewLine();
				}else{
					throw SyntaxError("Unexpected token: ", current());
				}
			}else if(current().punctuator == Punctuators::ASSIGN){
				nextNonNewLine();

				Node *exp = handleExpression(Punctuators::ASSIGN, false);

				if(exp == NULL)
					throw SyntaxError("Unexpected token: ", current());
				else asnList.at(asnDepth).push_back(exp);

			}else if(current().punctuator == Punctuators::ARGUMENT_DELIMITER && !InFuncCall){
				asnDepth++;
				asnList.push_back(vector<Node*>());
				nextNonNewLine();
				//asnList.at(asnDepth).push_back(handleExpression(Punctuators::ARGUMENT_DELIMITER, false));
				idenFound = false;
			}
			else if(isStopper(current())) break;

			else if(current().type == TokenType::EOF_EOF) {
				if(!idenFound)
					SyntaxError("Unexpected token: ", current());
				else break;
			}
			else{
				if(back(1).type == TokenType::LINE_TERMINATOR) break;
				else throw SyntaxError("Unexpected token: ", current());
			}
		}

		//Node *asn = asnList.back(); asnList.pop_back();
		InAssign = false;
		return new Assignment(asnList, isVar);
	}


	Node* Parser::handleWord(){
		Token to = current();
		switch(to.word){

		case KeyWord::VAR: return handleAssignment(true, NULL);
		default: return NULL;
		}
	}

	Node* Parser::handleObjectLit(){
		Token to = current();
		if(!(current().punctuator == Punctuators::BlOCK_START && (InAssign || InFuncCall))) return NULL;

		JSObject * _obj = new JSObject();
		std::string current_key;
		bool canEnd = true;

		nextNonNewLine();

		while(true){
			Token to = current();
			if(current().type != TokenType::IDENTIFIER && current().type != TokenType::STRING_LITERAL
					&& current().type != TokenType::NUMBER_LITERAL){
				if(current().punctuator == Punctuators::BLOCK_END && canEnd)
					break;
				unexpected(current());
			}
			else
				current_key = current().literalValue;

			nextNonNewLine();
			if(current().punctuator != Punctuators::PART_CONDITIONAL_OPERATOR)
				unexpected(current());

			nextNonNewLine();

			Node *exp = handleExpression(Punctuators::STATMENT_TERMINATOR, false);


			if(exp == NULL)
				unexpected(current());

			_obj->addField(current_key, exp);
			canEnd = true;
			if(current().punctuator == Punctuators::BLOCK_END)
				break;
			else if(current().punctuator == Punctuators::ARGUMENT_DELIMITER){
				nextNonNewLine();
				canEnd = false;
				continue;
			}else
				unexpected(current());
		}

		nextNonNewLine();
		return _obj;
	}


	Node* Parser::handleTerm(){
		Token to = current();

		switch(to.type){
		case TokenType::NUMBER_LITERAL:
			nextNonNewLine();
			return WrapNumber(to.literalValue);
		case TokenType::STRING_LITERAL:
			nextNonNewLine();
			return WrapString(to.literalValue);
		case TokenType::BOOLEAN_LITERAL:
			nextNonNewLine();
			return WrapBoolean(to.boolValue);
		case TokenType::IDENTIFIER:
			nextNonNewLine();
			return new IdentifierExpr(to.literalValue);
		case TokenType::UNDEFINED_LITERAL:
			nextNonNewLine();
			return new JSUndefined();
		default: return NULL;
		}
	}


	Node* Parser::handleBinaryExpr(int terminator, Node *firstNode){
		InBinOp = true;
		Token to = current();
		std::deque<Node*> nodes;
		deque<Node*> operators;
		int parenDepth = 0;
		bool firstParen = false;
		//bool isBinOpExp = false;

		if(firstNode != NULL){
			nodes.push_back(firstNode);
			//isBinOpExp = true;
		}

		while(to.type != TokenType::EOF_EOF && to.type != TokenType::NONE){

			if(to.punctuator == terminator) break;

			if(IsBinOperant(to)){
				//isBinOpExp = false;
				while((!operators.empty()) && IsBinOperant(operators.front()->ToAPunctuator()->getPunc())
						&&  GetWeight(operators.front()->ToAPunctuator()->getPunc()) <= GetWeight(to.punctuator)){
					nodes.push_back(operators.front());
					operators.pop_front();
				}
				operators.push_front(new APunctuator(to.punctuator));
				to = nextNonNewLine();
				continue;
			}/*else{
				if(current().punctuator != Punctuators::OPEN_PARENTHESES && current().punctuator != Punctuators::CLOSE_PERENTHESES){
					if(back(1).type == TokenType::LINE_TERMINATOR || to.punctuator == Punctuators::STATMENT_TERMINATOR
							||to.punctuator == Punctuators::ARGUMENT_DELIMITER) break;
					else unexpected(to);
				}
			}*/
			if(to.punctuator == Punctuators::OPEN_PARENTHESES){
				if(parenDepth <= 0 && firstParen) break;
				parenDepth++;
				firstParen = true;
				operators.push_front(new APunctuator(to.punctuator));
			}
			else if(to.punctuator == Punctuators::CLOSE_PERENTHESES){
				if(parenDepth <= 0) break;

				parenDepth--;
				while(!operators.empty() && operators.front()->ToAPunctuator()->getPunc() != Punctuators::OPEN_PARENTHESES){
					nodes.push_back(operators.front());
					operators.pop_front();
				}
				if(!operators.empty()) operators.pop_front();
				else throw SyntaxError("Parentheses mismatch.", current());
			}
			else{
				//isBinOpExp = true;
				Node *exp = handleExpression(terminator, true);
				if(exp != NULL){
					nodes.push_back(exp);
					to = current(); continue;
				}
				break;
			}
			to = nextNonNewLine();
		}

		while(!operators.empty()){
			auto op = operators.front()->ToAPunctuator();
			if(op->getPunc() == Punctuators::OPEN_PARENTHESES ||
					op->getPunc() == Punctuators::CLOSE_PERENTHESES){
				throw SyntaxError("Unexpected Token: "+Punctuators::From(op->getPunc()), Token());
			}
			nodes.push_back( operators.front() );
			operators.pop_front();
		}

		InBinOp = false;
		return new JSBinaryExpr(nodes);
	}

	Node* Parser::handleFunctionCall(Node *ref){
		InFuncCall = true;
		Token to  = current();
		//std::string funcName = current().literalValue;
		std::vector<Node*> args;
		Token close = peek();
		if(peek().punctuator != Punctuators::CLOSE_PERENTHESES){
			do{
				nextNonNewLine();
				args.push_back(handleExpression(Punctuators::ARGUMENT_DELIMITER, false));
			}while(current().punctuator == Punctuators::ARGUMENT_DELIMITER);
			to = current();
			if(current().punctuator != Punctuators::CLOSE_PERENTHESES)
				throw SyntaxError("Unexpected token: ", current());
		}else nextNonNewLine();

		nextNonNewLine();
		InFuncCall = false;
		return new CallExpr(ref, args);
	}

	bool Parser::IsBinOperant(int to){
		switch(to){
		case Punctuators::PLUS: case Punctuators::MINUS: case Punctuators::MULTIPLY: case Punctuators::DIVIDE:
		case Punctuators::EQUAL_TO: case Punctuators::GREATER_THAN: case Punctuators::LESS_THAN: case Punctuators::REMAINDER:
			return true;
		default: return false;
		}
	}

	bool Parser::IsBinOperant(Token to){
		return IsBinOperant(to.punctuator);
	}

	JSString* Parser::WrapString(std::string a){
		return new JSString(a);
	}

	JSNumber* Parser::WrapNumber(std::string a){
		return new JSNumber(atof(a.c_str()));
	}

	JSBool* Parser::WrapBoolean(bool value){
		return new JSBool(value);
	}

	/*
	void Parser::parseFunction(){
		Token to;
		std::string funcName;
		if(peek().type != TokenType::IDENTIFIER){
			//TODO: handle anonymous function
			Error("Expected token: identifier.");
		}

		to = next(); funcName = to.literalValue;

		if(peek().punctuator == Punctuators::OPEN_PARENTHESES){
			next();
		}else{
			Error("Expected token: '('");
		}

		if(current().punctuator != Punctuators::CLOSE_PERENTHESES){

			while(true){
				Expr * arg = handleFuncArgs();
			}
		}
	}
	*/

	void Parser::expect(int type, int token){
		Token cu = current();

		switch(type){
		case TokenType::PUNCUATOR:
			if(cu.punctuator != token) unexpected(cu);
			break;
		case TokenType::KEYWORD:
			if(cu.word != token) unexpected(cu);
			break;
		default: unexpected(cu);
		}
	}

	bool Parser::isStopper(Token to){
		switch(to.punctuator){
		case Punctuators::STATMENT_TERMINATOR: case Punctuators::ARGUMENT_DELIMITER: case Punctuators::CLOSE_PERENTHESES:
		case Punctuators::BLOCK_END:
			return true;
		}
		if(to.type == TokenType::EOF_EOF) return true;
		return false;
	}
	Token Parser::backNonNL(){
		int depth = 1;
		while(back(depth).type == TokenType::LINE_TERMINATOR && back(depth).type != TokenType::NONE) depth++;

		return tokens.at(depth);
	}
	Token Parser::nextNonNewLine(){
		next();
		while(current().type == TokenType::LINE_TERMINATOR){
			lastIsNewLine = true;
			next();
		}
		return current();
	}

	Token Parser::nextSkipSemicolon(){
		nextNonNewLine();
		while(current().punctuator == Punctuators::STATMENT_TERMINATOR &&
				current().type != TokenType::EOF_EOF) nextNonNewLine();
		return current();
	}

	Token Parser::current(){
		return tokens.at(pos);
	}

	Token Parser::next(){
		if(pos+1 < tokens.size())
			return tokens.at(++pos);
		return Token();
	}

	Token Parser::peekNonNL(int run){
		size_t n = pos+1;
		while(n < tokens.size() && tokens.at(n).type == TokenType::LINE_TERMINATOR) n++;
		return tokens.at(n);
	}

	Token Parser::peek(){
		if(pos+1 < tokens.size())
			return tokens.at(1+pos);
		return Token();
	}

	Token Parser::back(int i = 1){
		if(pos-i >= 0)
			return tokens.at(pos-i);
		return Token();
	}

	int Parser::GetWeight(int a){
		if(a == Punctuators::OPEN_PARENTHESES || a == Punctuators::CLOSE_PERENTHESES)
			return 0;
		return opPrecedence.at(a);
	}

	void Parser::unexpected(Token to){
		throw SyntaxError("Unexpected token: ", to);
	}

	void Parser::Error(std::string str){
		std::cout << str << std::endl;
		throw str;
	}

}
