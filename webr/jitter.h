
#ifndef WEBR_CodeGen
#define WEBR_CodeGen
#include "stack.h"

namespace webr {
	namespace Internal {
		class Jitter {
		public:

			Jitter();

			void Run(char* code);
		private:
			//void* stackHead;
			char* codeBuff;
			int codePos;
			//int codeSize;

			intptr_t DoOp(char op, int16_t v1, int16_t v2);
			void OpVN(char op);
			void OpVV(char op);
			void OpNV(char op);
			void set(int16_t slot, intptr_t objPtr);
			void setun();
			void setbool();
			void setnum();
			void addvv();
			void addnv();
			void addvn();
			void subvn();
			void subvv();
			void subnv();
			void divvv();
			void divvn();
			void divnv();
			void mulvv();
			void mulvn();
			void mulnv();
			void copyr();
			void expect(unsigned char i);

			void* lookup(int16_t var);
			int16_t byte2To16Bit(unsigned char a, unsigned char b);
			unsigned char nextByte();
			intptr_t nextAsObjectPointer();
			int16_t nextAs16BitNum();

			char nextAs8BitNum();

			void changeStack(void* p);
			void call();
			//std::unordered_map<char, int> r;
			intptr_t currentStack;
			void* constantTable;

		};
	}
}

#endif //WEBR_CodeGen
