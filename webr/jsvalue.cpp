
#include "jsvalue.h"
#include "const.h"
namespace webr {
	using namespace Internal;
	JSValue::JSValue(int type) : Node(NodeType::VALUE){
		this->type = type;
	}

	JSValue::~JSValue(){
		delete &type;
		delete this;
	}

	bool JSValue::IsString(){
		return type == VariableType::STRING;
	}

	bool JSValue::IsNumber(){
		return type == VariableType::NUMBER;
	}

	bool JSValue::IsObject(){
		return type == VariableType::OBJECT;
	}

	bool JSValue::IsArray(){
		return false;
	}

	bool JSValue::IsFunction(){
		return type == VariableType::FUNCTION;
	}

	bool JSValue::IsRegex(){
		return false;
	}

	bool JSValue::IsBoolean(){
		return type == VariableType::BOOLEAN;
	}

	bool JSValue::IsUndefined(){
		return type == VariableType::UNDEFINED;
	}

	JSString* JSValue::CastString(){ return (JSString*)this; }

	JSNumber* JSValue::CastNumber(){ return (JSNumber*)this; }

	JSNumber* JSValue::ToNumber(){
		if(IsNumber())
			return CastNumber();
		return 0;
	}

	JSString* JSValue::ToString(){
		if(IsString())
			 return CastString();
		//if(IsNumber())
			//return std::string("s");
		return 0;
	}

	JSFunction* JSValue::ToFunction(){
		if(IsFunction())
			return (JSFunction*)this;
		return NULL;
	}

	JSBool* JSValue::ToBoolean(){
		if(IsBoolean())
			return (JSBool*)this;
		return NULL;
	}

	JSObject* JSValue::ToObject(){
		return IsObject() ? (JSObject*)this : NULL;
	}

	int JSValue::ValueType(){
		return type;
	}

	std::string JSValue::operator<<(JSValue *s){
		return s->ToStringValue();
	}

}
