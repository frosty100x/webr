
#include "xparser.h"
namespace webr {

	namespace Internal {
		using namespace Enumerations;
		xNode* StaticOp::DoOperation(Punctuator op, xNode *lhs, xNode *rhs) {

			switch (op) {
			case Punctuator::PLUS:
				return DoPlusOperation(lhs, rhs);
			case Punctuator::MINUS:
				return DoSubtractOperation(lhs, rhs);
			case Punctuator::DIVIDE:
				return DoDivideOperation(lhs, rhs);
			case Punctuator::MULTIPLY:
				return DoMultiplyOperation(lhs, rhs);
			case Punctuator::REMAINDER:
				return DoModulusOperation(lhs, rhs);
			}
			return NULL;
		}

		xNode* StaticOp::DoBasicOp(xNode *lhs, xNode *rhs, OpDelegate op) {
			if (lhs->type == VARTYPE::CONSTANT_16BIT || rhs->type == VARTYPE::CONSTANT_16BIT) {
				return new xNode{ VARTYPE::CONSTANT_16BIT, true,
					(intptr_t) new int16_t(op(lhs, rhs))
				};
			}
			else {
				return NULL;
				//TODO: return NaN
			}
		}

		xNode* StaticOp::DoPlusOperation(xNode *lhs, xNode *rhs) {

			if (lhs->type == VARTYPE::CONSTANT_16BIT) {

				if (rhs->type == VARTYPE::CONSTANT_16BIT)
					return new xNode{ VARTYPE::CONSTANT_16BIT, true,
					(intptr_t) new int16_t((*(int16_t*)lhs->value) + (*(int16_t*)rhs->value))
				};
			}
		}

		xNode* StaticOp::DoSubtractOperation(xNode *lhs, xNode *rhs) {
			return DoBasicOp(lhs, rhs, Sub);
		}
		xNode* StaticOp::DoMultiplyOperation(xNode *lhs, xNode *rhs) {
			return DoBasicOp(lhs, rhs, Mul);
		}
		xNode* StaticOp::DoDivideOperation(xNode *lhs, xNode *rhs) {
			return DoBasicOp(lhs, rhs, Div);
		}
		xNode* StaticOp::DoModulusOperation(xNode *lhs, xNode *rhs) {
			return DoBasicOp(lhs, rhs, Mod);
		}

		int16_t StaticOp::Sub(xNode *lhs, xNode *rhs) {
			return (*(int16_t*)lhs->value) - (*(int16_t*)rhs->value);
		}
		int16_t StaticOp::Mul(xNode *lhs, xNode *rhs) {
			return (*(int16_t*)lhs->value) * (*(int16_t*)rhs->value);
		}
		int16_t StaticOp::Div(xNode *lhs, xNode *rhs) {
			return (*(int16_t*)lhs->value) / (*(int16_t*)rhs->value);
		}
		int16_t StaticOp::Mod(xNode *lhs, xNode *rhs) {
			return (*(int16_t*)lhs->value) % (*(int16_t*)rhs->value);
		}

	}

}