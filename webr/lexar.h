
#ifndef WEBR_LEXAR
#define WEBR_LEXAR
#include <string>
#include <vector>
#include "const.h"
namespace webr {

	namespace Internal {

		using namespace Enumerations;
		
		class Lexar {

		public:
			Lexar();
			~Lexar();
			Lexar(std::string data);
			std::vector<Token> parse();

			void set(std::string value);

		private:
			int currentLine;
			std::string data;
			size_t pos;
			int dataLength;
			std::vector<Token> tokens;
			char readEscape();
			void readNumber();
			void readIllegal();
			void reset(int pos);
			char peek();
			char next();
			char current();
			char back();
			bool IsOperator(char c);
			KeyWord IsKeyword(std::string word);
			bool IsLineTerm(char c);
			bool IsQuote(char c);
			bool IsPunctuator(char c);

			void readPunctuator();
			void readOperator();
			void readWord();
			void readLineTerm();
			void readString();
			void readComment();
			Punctuator readEqualPostfix();
			Punctuator readLTPostfix();
			Punctuator readAmpPostfix();
			Punctuator readVertBarPostfix();
			Punctuator readGTPostfix();
			Punctuator readExclPostfix();
			Punctuator readPlusPostfix();
			Punctuator readMinusPostfix();

		};
	}
}
#endif //WEBR_LEXAR
