
#include "jsbool.h"
#include "const.h"

namespace webr {
	using namespace Internal::Enumerations;
	JSBool::JSBool(bool value) : JSValue(VariableType::BOOLEAN) {
		this->itr_value = value;
	}

	JSBool::~JSBool(){
		delete &itr_value;
	}

	JSValue* JSBool::getValue(Engine *e, Routine *owner){
		return this;
	}

	std::string JSBool::ToStringValue(){
		return itr_value ? "true" : "false";
	}

	bool JSBool::ToBooleanValue(){
		return itr_value;
	}

	void JSBool::Dispose(){
		delete &itr_value;
		delete this;
	}

}
