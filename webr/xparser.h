#ifndef WEBR_XPARSER
#define WEBR_XPARSER
#include "lexar.h"
#include "const.h"
#include <string>
#include <map>
#include <cstdint>
#include <deque>
#include <initializer_list>
#include "huballocator.h"
#include "codegen.h"

namespace webr{

	namespace Internal {
		//class Lexar;
		using namespace JSObjects;

		struct xNode {
			VARTYPE type;
			bool isConstant;
			intptr_t value;
		};

		class xParser {
		public:
			xParser(int maxSlots);
			CodeBuff* parse(std::string source);

		private:
			Lexar *lex;
			std::vector<Enumerations::Token> tokens;
			CodeGen *codegen;

			struct BinOpNode{
				int isPunc;
				int value;
			};

			std::map<Enumerations::Punctuator, int> opPrecedence = {
				{Enumerations::Punctuator::OPEN_PARENTHESES, 1},
				{Enumerations::Punctuator::CLOSE_PERENTHESES, 1},
				{Enumerations::Punctuator::MULTIPLY, 2},
				{Enumerations::Punctuator::DIVIDE, 2},
				{Enumerations::Punctuator::REMAINDER, 2},
				{Enumerations::Punctuator::PLUS, 3},
				{Enumerations::Punctuator::MINUS, 3},
				{Enumerations::Punctuator::SHIFT_LEFT, 4},
				{Enumerations::Punctuator::SHIFT_RIGHT, 4},
				{Enumerations::Punctuator::SHIFT_RIGHT_UNSIGNED, 4},
				{Enumerations::Punctuator::LESS_THAN, 5},
				{Enumerations::Punctuator::LT_EQUAL, 5},
				{Enumerations::Punctuator::GREATER_THAN, 5},
				{Enumerations::Punctuator::GT_EQUAL, 5},
				{Enumerations::Punctuator::EQUAL_TO, 6},
				{Enumerations::Punctuator::NOT_EQUAL_TO, 6},
				{Enumerations::Punctuator::PERCISION_EQUAL_TO, 6},
				{Enumerations::Punctuator::PERCISION_NOT_EQUAL_TO, 6},
				{Enumerations::Punctuator::BITWISE_AND, 7},
				{Enumerations::Punctuator::BITWISE_XOR, 8},
				{Enumerations::Punctuator::BITWISE_OR, 9},
				{Enumerations::Punctuator::AND, 10},
				{Enumerations::Punctuator::OR, 11},
			};

			bool InBinOp;
			bool InAssign;
			bool InFuncCall;

			bool lastIsNewLine;
			size_t pos;
			int slotCounter;
			int maxSlots;
			int bufferPos;
			char* buffer;

			std::map<std::string, int16_t> varMap;

			void handle();

			xNode* handleExpression();
			xNode* handleSecondaryExpr(xNode *lastNode);
			xNode* handleBinaryExpr(xNode *firstSlot);
			xNode* handleAssignment(bool isVar, xNode *firstNode);
			xNode* handleWord();
			xNode* handleTerm();

			xNode* BinOpCodeGen(std::deque<xNode*> &nodes);
			int DoOperation(Punctuator op, int value1, int value2);

			bool IsBinOperant(Enumerations::Token to);
			xNode* DoPlusOperation(xNode * lhs, xNode * rhs);
			int DoOperation(Punctuator op, xNode v1, int v2);
			bool IsBinOperant(Punctuator to);
			bool IsStopper(Enumerations::Token to);
			bool IsStatmentTerm();

			Enumerations::Token current();
			Enumerations::Token next();
			Enumerations::Token back(int i);
			int16_t nextSlot();
			int GetWeight(Punctuator a);
			void putByte(char byte);
			void putBytes(char bytes[]);
			void putInt16(int16_t value);

			Enumerations::Token backNonNL();
			void unexpected(Enumerations::Token to);
			Enumerations::Token nextNonNewLine();
			Enumerations::Token nextSkipSemicolon();

			bool IsUnary(Punctuator punc);
			int16_t GetVarNum(std::string v);
			xNode * WrapBoolean(bool value);
			xNode* WrapIden(std::string a);
			xNode* WrapString(const char* str);
			xNode* WrapNumber(std::string a);
			VARTYPE SmallestInt(int64_t value);
			void PuncToOpCode(Punctuator punc, xNode *v1Con, xNode *v2Con);
			void PlaceOperation(OpCode op, xNode * lhs, xNode * rhs);
		};

		static class StaticOp {
			
		private:
			typedef int16_t(OpDelegate)(xNode *lhs, xNode *rhs);

		public:
			static xNode* DoOperation(Punctuator op, xNode *lhs, xNode *rhs);
			static xNode * DoBasicOp(xNode * lhs, xNode * rhs, OpDelegate op);
			static xNode* DoPlusOperation(xNode *lhs, xNode *rhs);
			static xNode* DoSubtractOperation(xNode *lhs, xNode *rhs);
			static xNode* DoMultiplyOperation(xNode *lhs, xNode *rhs);
			static xNode* DoDivideOperation(xNode *lhs, xNode *rhs);
			static xNode * DoModulusOperation(xNode * lhs, xNode * rhs);
			static int16_t Sub(xNode * lhs, xNode * rhs);
			static int16_t Mul(xNode * lhs, xNode * rhs);
			static int16_t Div(xNode *lhs, xNode *rhs);
			static int16_t Mod(xNode * lhs, xNode * rhs);
		};

	}

}

#endif
