#include "xparser.h"
#include "errortypes.h"
#include <iostream>
#include <deque>
#include <stack>
#include <initializer_list>
#include <cstdlib>
#include <stdexcept>
namespace webr {

	namespace Internal {

		using namespace Enumerations;
		typedef void (*vn)(char op, slot destSlot, slot slot, intptr_t var);
		typedef void (*nv)(char op, slot destSlot, intptr_t var, slot slot);

		xParser::xParser(int maxSlots){
			this->maxSlots = maxSlots;
			//this->tokens = _tokens;
			//slot 1 will always hold the return value
			//of the last expression.
			slotCounter = 1;
			this->pos = 0;
			this->bufferPos = 0;
			this->InAssign = false;
			this->InBinOp = false;
			this->InFuncCall = false;
			this->lastIsNewLine = false;
			lex = new Lexar();
			codegen = new CodeGen();
			//lex->set(source);
			//this->tokens = lex->parse();

			//codegen->buffer = (char*)HubAllocator::getSpace(100);
		}


		 CodeBuff* xParser::parse(std::string source){
			lex->set(source);
			this->tokens = lex->parse();
			while(pos+1 < tokens.size()){
				handle();
			}

			return codegen->GetBuffer();
		}

		void xParser::handle(){
			Token to = current();
			xNode *lastNode;

			if(to.type == TokenType::EOF_EOF || to.punc == Punctuator::STATMENT_TERMINATOR
					|| to.type == TokenType::LINE_TERMINATOR){
				nextSkipSemicolon();
				return;
			}
			/*
			if(Node *n = handleBlock()) return n;

			if(Node *n = handleFork())
				return n;

			if(Node *n = handleIfSt())
				return n;

			if(Node *n = handleWhileST())
				return n;
			*/

			if((lastNode = handleExpression()))
				return;

			unexpected(current());
		}

		xNode* xParser::handleExpression(){
			Token to = current();
			xNode *lastNode;// = nullptr;
			xNode *result;
			/*

			if(to.punctuator == Punctuator::STATMENT_TERMINATOR){
				//nextNonNewLine();
				return NULL;
			}

			else if(to.punctuator == terminator){
			//	nextNonNewLine();
				return NULL;
			}

			else if((lastExpr = handleObjectLit())){}
			 */
			if((lastNode = handleWord())){}

			else if((lastNode = handleTerm())){}

			for(;;){
				result = handleSecondaryExpr(lastNode);
				if(result != NULL)
					lastNode = result;
				else break;
			}

			if(to.punc == Punctuator::OPEN_PARENTHESES)
				return handleBinaryExpr(NULL);

			return lastNode;
		}

		xNode* xParser::handleSecondaryExpr(xNode *lastNode){
			Token to = current();
			/*
			if(to.punctuator == Punctuator::OPEN_PARENTHESES){
				Node* t = lastExpr;
				Node *tmp = handleFunctionCall(t);
				lastExpr = (tmp != NULL) ? tmp : lastExpr;
			}else if(!InAssign && to.punctuator == Punctuator::ASSIGN){
				Node *tmp = handleAssignment(false, lastExpr);
				 lastExpr = (tmp != NULL) ? tmp : lastExpr;
			}else if(to.punctuator == Punctuator::STATMENT_TERMINATOR){
				return false;
			}else if(to.punctuator == Punctuator::PROPERTY_ACCESSOR){
				Node *tmp = handleModifier(lastExpr);
				lastExpr = (tmp != NULL) ? tmp : lastExpr;
				return true;
			}*/

			if(IsBinOperant(to) && !InBinOp){
				return handleBinaryExpr(lastNode);
				//return true;
			}

			//to = current();

			//if(!isStopper(to)){
				//if(back(1).type != TokenType::LINE_TERMINATOR)
					//throw SyntaxError("Unexpected token: ", to);
			//}
			return NULL;
		}

		xNode* xParser::handleWord(){
			Token to = current();
			switch(to.word){

			case KeyWord::VAR: return handleAssignment(true, NULL);
			default: return NULL;
			}
		}

		xNode* xParser::handleAssignment(bool isVar, xNode *firstNode){
			if(isVar && (InFuncCall || InBinOp)) return NULL;
			InAssign = true;
			Token to = current();
			std::vector<std::vector<xNode*>> asnList;
			int asnDepth = 0;
			bool idenFound = false;
			asnList.push_back(std::vector<xNode*>());

			std::vector<xNode*> assignSlots;

			auto groupUnload = [this, &assignSlots](){

				if(assignSlots.size() == 1){
					codegen->setunf(assignSlots.back()->value);
				}
				else do{
					xNode *last = assignSlots.back(); assignSlots.pop_back();
					xNode *slast = assignSlots.back();

					if (last->isConstant)
						codegen->setslot(slast->value, last);
					else
						codegen->copyref(slast->value, last->value);
				}while(assignSlots.size() > 1);
				assignSlots.clear();
			};

			if(!isVar){
				idenFound = true;
				asnList.at(0).push_back(firstNode);
			}else
				nextNonNewLine();

			while(current().type != TokenType::NONE){
				if(!idenFound){
					idenFound = true;
					if(current().type == TokenType::IDENTIFIER){
						xNode *_exp = WrapIden(current().literalValue);
						assignSlots.push_back(_exp);
						nextNonNewLine();
					}else{
						throw SyntaxError("Unexpected token: ", current());
					}
				}else if(current().punc == Punctuator::ASSIGN){
					nextNonNewLine();
					xNode *exp = handleExpression();

					if(exp == NULL)
						throw SyntaxError("Unexpected token: ", current());
					else
						assignSlots.push_back(exp);

				}else if(current().punc == Punctuator::ARGUMENT_DELIMITER && !InFuncCall){
					groupUnload();
					nextNonNewLine();
					idenFound = false;
				}
				else if(IsStatmentTerm()) break;
				//else if(IsStopper(current())) break;

				else if(current().type == TokenType::EOF_EOF) {
					if(!idenFound)
						SyntaxError("Unexpected token: ", current());
					else break;
				}
				else{
					if(back(1).type == TokenType::LINE_TERMINATOR) break;
					else throw SyntaxError("Unexpected token: ", current());
				}
			}

			//TODO: if assignSlots == 0 then set to undefined
			groupUnload();
			//Node *asn = asnList.back(); asnList.pop_back();
			InAssign = false;
			return new xNode{VARTYPE::SLOT_NUMBER, false, 1};
			//return new Assignment(asnList, isVar);
		}

		xNode* xParser::handleTerm(){
			Token to = current();

			switch(to.type){
			case TokenType::NUMBER_LITERAL:
				nextNonNewLine();
				return WrapNumber(to.literalValue);
			case TokenType::IDENTIFIER:
				nextNonNewLine();
				return WrapIden(to.literalValue);

			case TokenType::STRING_LITERAL:
				nextNonNewLine();
				return WrapString(to.literalValue.c_str());
				
			case TokenType::BOOLEAN_LITERAL:
				nextNonNewLine();
				return WrapBoolean(to.boolValue);

				/*
			case TokenType::UNDEFINED_LITERAL:
				nextNonNewLine();
				return new JSUndefined();
				*/
			default: return NULL;
			}
		}

		xNode* xParser::handleBinaryExpr(xNode *firstTerm){
			InBinOp = true;
			Token to = current();
			//std::deque<Node*> nodes;
			std::deque<xNode*> nodes;

			//deque<Node*> operators;
			std::deque<xNode*> operators;

			int parenDepth = 0;
			bool firstParen = false;
			bool needPunc = false;

			if(firstTerm != 0){
				nodes.push_back(firstTerm);
				needPunc = true;
			}

			while(to.type != TokenType::EOF_EOF && to.type != TokenType::NONE){
				//if(to.punctuator == terminator) break;

				if(IsBinOperant(to)){
					needPunc = false;
					while((!operators.empty()) && IsBinOperant((Punctuator)operators.front()->value)
							&&  GetWeight((Punctuator)operators.front()->value) <= GetWeight(to.punc)){
						nodes.push_back(operators.front());
						operators.pop_front();
					}
					operators.push_front(new xNode{VARTYPE::PUNCUATOR, false, (intptr_t)to.punc });
					to = nextNonNewLine();
					continue;
				}/*else{
					if(current().punctuator != Punctuator::OPEN_PARENTHESES && current().punctuator != Punctuator::CLOSE_PERENTHESES){
						if(back(1).type == TokenType::LINE_TERMINATOR || to.punctuator == Punctuator::STATMENT_TERMINATOR
								||to.punctuator == Punctuator::ARGUMENT_DELIMITER) break;
						else unexpected(to);
					}
				}*/
				if(to.punc == Punctuator::OPEN_PARENTHESES){
					if(parenDepth <= 0 && firstParen) break;
					parenDepth++;
					firstParen = true;
					operators.push_front(new xNode{VARTYPE::PUNCUATOR, false, (intptr_t)to.punc });
				}
				else if(to.punc == Punctuator::CLOSE_PERENTHESES){
					if(parenDepth <= 0) break;

					parenDepth--;
					while(!operators.empty() && operators.front()->value != (int)Punctuator::OPEN_PARENTHESES){
						nodes.push_back(operators.front());
						operators.pop_front();
					}
					if(!operators.empty()) operators.pop_front();
					else throw SyntaxError("Parentheses mismatch.", current());
				}
				else{
					if(needPunc) break;
					needPunc = true;
					xNode *exp = handleExpression();
					if(exp != NULL){
						nodes.push_back(exp);
						to = current(); continue;
					}
					break;
				}
				to = nextNonNewLine();
			}

			while(!operators.empty()){
				auto op = operators.front()->value;
				if(op == (int)Punctuator::OPEN_PARENTHESES ||
						op == (int)Punctuator::CLOSE_PERENTHESES){
					throw SyntaxError("Unexpected Token: " + FromPunctuator((Punctuator)op), Token());
				}
				nodes.push_back( operators.front() );
				operators.pop_front();
			}

			InBinOp = false;
			return BinOpCodeGen(nodes);
		}

		xNode* xParser::BinOpCodeGen(std::deque<xNode*> &nodes){
			//std::vector<xNode> stack;
			std::deque<xNode*> stack;
			//std::deque<Operation> byteStack;

			//int16_t expSlot = nextSlot();
			for(size_t i=0; i<nodes.size(); i++){
				xNode *n1 = nodes.at(i);

				if(n1->type == VARTYPE::PUNCUATOR){

					if(stack.empty()){
						throw SyntaxError("error");
					}

					else if(stack.size() == 1){
						//if(!IsUnary(n1->value))
						//	throw SyntaxError("error");

						//xNode node = stack.back(); stack.pop_back();
						//lastPunc = node.value;
						//stack.push_back(UnaryEval(n->ToAPunctuator()->getPunc(), value->ToJSValue()));
					}else{
						xNode *rhs = stack.back(); stack.pop_back();
						xNode *lhs = stack.back(); stack.pop_back();

						if(rhs->isConstant && lhs->isConstant){
							xNode* result = StaticOp::DoOperation((Punctuator)n1->value, lhs, rhs);
							//TODO: if result < a 16bit int embbed else slot number
							stack.push_back(result);
						}else{
							PuncToOpCode((Punctuator)n1->value, lhs, rhs);
							stack.push_back(new xNode{VARTYPE::SLOT_NUMBER, false, 1});
						}
					}
				}else{
					stack.push_back(n1);
				}
			}

			xNode *re = stack.back();

			if(re->isConstant)
			{
				//create int_var and point slot 1 to it
				codegen->setnum(1, re->value);
				return new xNode{VARTYPE::SLOT_NUMBER, false, 1};
			}else{ //if slot
				//point slot 1 to it
				return new xNode{VARTYPE::SLOT_NUMBER, false, 1};
			}
		}

		void xParser::PuncToOpCode(Punctuator punc, xNode *lhs, xNode *rhs){
			switch(punc){
			case Punctuator::PLUS:
				if(lhs->isConstant)
					codegen->putnv(OpCode::ADDNV, 1, lhs->value, rhs->value);
				else if(rhs->isConstant)
					codegen->putvn(OpCode::ADDVN, 1, lhs->value, rhs->value);
				else
					codegen->putvv(OpCode::ADDVV, 1, lhs->value, rhs->value);
				break;
			case Punctuator::MINUS:
				if(lhs->isConstant)
					codegen->putnv(OpCode::SUBNV, 1, lhs->value, rhs->value);
				else if(rhs->isConstant)
					codegen->putvn(OpCode::SUBVN, 1, lhs->value, rhs->value);
				else
					codegen->putvv(OpCode::SUBVV, 1, lhs->value, rhs->value);
				break;
			case Punctuator::MULTIPLY:
				if(lhs->isConstant)
					codegen->putnv(OpCode::MULNV, 1, lhs->value, rhs->value);
				else if(rhs->isConstant)
					codegen->putvn(OpCode::MULVN, 1, lhs->value, rhs->value);
				else
					codegen->putvv(OpCode::MULVV, 1, lhs->value, rhs->value);
				break;
			case Punctuator::DIVIDE:
				if(lhs->isConstant)
					codegen->putnv(OpCode::DIVNV, 1, lhs->value, rhs->value);
				else if(rhs->isConstant)
					codegen->putvn(OpCode::DIVVN, 1, lhs->value, rhs->value);
				else
					codegen->putvv(OpCode::DIVVV, 1, lhs->value, rhs->value);
				break;
			}
		}

		VARTYPE xParser::SmallestInt(int64_t value) {
			if(value <= 256)
				return VARTYPE::CONSTANT_8BIT;
			else if(value <= 65536)
				return VARTYPE::CONSTANT_16BIT;
			else if(value <= 4294967296)
				return VARTYPE::CONSTANT_32BIT;
			else //if(value <= 18446744073709552000);
				return VARTYPE::CONSTANT_64BIT;
		}

		bool xParser::IsBinOperant(Punctuator to){
			switch(to){
			case Punctuator::PLUS: case Punctuator::MINUS: case Punctuator::MULTIPLY: case Punctuator::DIVIDE:
			case Punctuator::EQUAL_TO: case Punctuator::GREATER_THAN: case Punctuator::LESS_THAN: case Punctuator::REMAINDER:
				return true;
			default: return false;
			}
		}

		bool xParser::IsBinOperant(Token to){
			return IsBinOperant(to.punc);
		}

		xNode* xParser::WrapIden(std::string a){
			return new xNode{
				VARTYPE::SLOT_NUMBER,
				false,
				GetVarNum(a)
			};
		}

		int16_t xParser::GetVarNum(std::string v){
			try{
				return varMap.at(v);
			}catch(std::out_of_range &e){
				slot sl = nextSlot();
				varMap[v] = sl;
				return sl;
			}
		}

		xNode* xParser::WrapBoolean(bool value) {
			xNode *_bool = new xNode;
			_bool->type = VARTYPE::CONSTANT_BOOLEAN;
			_bool->isConstant = true;
			_bool->value = value;
			return _bool;
		}

		xNode* xParser::WrapString(const char* str){
			xNode *string = new xNode;
			string->type = VARTYPE::CONSTANT_STRING;
			string->isConstant = true;
			string->value = (intptr_t)str;
			return string;
		}

		xNode* xParser::WrapNumber(std::string a){
			xNode *num = new xNode;
			num->isConstant = true;
			num->value = (intptr_t)new int16_t(atof(a.c_str()));
			num->type = VARTYPE::CONSTANT_16BIT; //SmallestInt(num->value);
			codegen->setnum(1, num->value);
			return num;
		}

		bool xParser::IsStopper(Token to){
			switch(to.punc){
			case Punctuator::STATMENT_TERMINATOR: case Punctuator::ARGUMENT_DELIMITER: case Punctuator::CLOSE_PERENTHESES:
			case Punctuator::BLOCK_END:
				return true;
			}
			if(to.type == TokenType::EOF_EOF) return true;
			return false;
		}

		inline bool xParser::IsStatmentTerm(){
			return current().punc == Punctuator::STATMENT_TERMINATOR;
		}

		void xParser::unexpected(Token to){
			throw SyntaxError("Unexpected token: ", to);
		}

		void xParser::putInt16(int16_t value){
			((int16_t*)buffer)[0] = value;
			buffer += sizeof(int16_t);
		}

		void xParser::putByte(char b){
			*buffer = b; buffer++;
		}

		void xParser::putBytes(char s[]){
			int size = sizeof(s);
			for(int i=0; i< size; i++)
				putByte(s[i]);
		}

		slot xParser::nextSlot(){
			return ++slotCounter;
		}

		Token xParser::backNonNL(){
			int depth = 1;
			while(back(depth).type == TokenType::LINE_TERMINATOR && back(depth).type != TokenType::NONE) depth++;

			return tokens.at(depth);
		}
		Token xParser::nextNonNewLine(){
			next();
			while(current().type == TokenType::LINE_TERMINATOR){
				lastIsNewLine = true;
				next();
			}
			return current();
		}

		Token xParser::nextSkipSemicolon(){
			nextNonNewLine();
			while(current().punc == Punctuator::STATMENT_TERMINATOR &&
					current().type != TokenType::EOF_EOF) nextNonNewLine();
			return current();
		}

		Token xParser::next(){
			if(pos+1 < tokens.size())
				return tokens.at(++pos);
			return Token();
		}

		int xParser::GetWeight(Punctuator a){
			if(a == Punctuator::OPEN_PARENTHESES || a == Punctuator::CLOSE_PERENTHESES)
				return 0;
			return opPrecedence.at(a);
		}

		Token xParser::current(){
			return tokens.at(pos);
		}

		Token xParser::back(int i){
			if(pos-i >= 0)
				return tokens.at(pos-i);
			return Token();
		}

		bool xParser::IsUnary(Punctuator punc){
			switch(punc){
			case Punctuator::PLUS: case Punctuator::MINUS: case Punctuator::NOT:
				return true;
			default: return false;
			}
		}



	}
}
