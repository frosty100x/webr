#ifndef WEBR_JSREFERENCE
#define WEBR_JSREFERENCE

#include <string>
#include "node.h"

namespace webr {
	class JSValue;
	class JSReference : public Node {
	public:
		JSReference(std::string name, JSValue *value);
		JSReference(std::string name);
		void Set(JSValue *v);

		JSValue * Value();
		std::string Name();

		JSValue* getValue(Engine *e, Routine *owner);

	private:
		JSValue *ref_object;
		std::string ref_name;

	};
}

#endif //WEBR_JSREFERENCE
