#ifndef WEBR_JSFUNCTION
#define WEBR_JSFUNCTION
#include <string>
#include "jsvalue.h"
#include "engine.h"
namespace webr {
	class JSValue;
	class JSFunction : public JSValue {
	public:
		JSFunction(Function func, std::string name);
		JSFunction(Function func);
		//~JSFunction();

		JSValue* getValue(Engine *e, Routine *owner);
		JSValue* Call(JSArgs *args, Engine *e);

		std::string ToStringValue();
		bool ToBooleanValue();
		void Dispose();
	private:
		bool IsNative;
		Function native_func;
		std::string func_name;

	};


}


#endif //WEBR_JSFUNCTION
