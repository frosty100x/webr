
#ifndef WEBR_ERRORTYPES
#define WEBR_ERRORTYPES

#include <exception>
#include <string>
#include "const.h"
namespace webr{
	using namespace Internal::Enumerations;
	class SyntaxError {
	public:
		SyntaxError(std::string mes, Token _to);
		SyntaxError(std::string mes);
		std::string message();

	private:
		std::string value(Token to);
		std::string mes;
		int lineNumber;
		Token to;
		//temporary
	    template <typename T>
	    std::string to_string(T value);
	};

	class ReferenceError{
	public:
		ReferenceError(std::string mes, int lineNumber);
		std::string message();

	private:
		Token to;
		std::string mes;
		int lineNumber;
	};

}

#endif //WEBR_ERRORTYPES
