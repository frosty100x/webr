#include <iostream>
#include "engine.h"
#include "parser.h"
#include "jsvalue.h"
#include "jscontext.h"
#include "jsnumber.h"
#include "jsreference.h"
#include "jsfunction.h"
#include "jsundefined.h"
namespace webr {

	CallExpr::~CallExpr(){}

	CallExpr::CallExpr(Node * _callee, std::vector<Node*> _args)
			: Expression(ExpressionType::FUNCTION_CALL), callee(_callee), args(_args){

	}

	int CallExpr::Type(){
		return ExpressionType::FUNCTION_CALL;
	}

	int CallExpr::BaseType(){
		return 0;
	}

	JSValue* CallExpr::calleeObject(){
		//if(callee == NULL) return NULL;

	//	if(callee->IsExpression())
	//	//	return callee->ToExpression()->getValue(e);
	//	else if(callee->IsJSValue())
	//		return callee->ToJSValue();
		//return callee->getValue(e, );
	}

	JSValue* CallExpr::getValue(Engine *e, Routine *owner){
		this->e = e;
		JSArgs *_args = buildArgs(owner);
		JSValue * _callee = callee->getValue(e, owner);//calleeObject();
		if(_callee->IsFunction())
			return _callee->ToFunction()->Call(_args, e);

		return (JSValue*)e->NewString("throw TypeError"); //TODO: throw TypeError in js
	}

	JSArgs* CallExpr::buildArgs(Routine *owner){
		JSArgs *_args = new JSArgs(e);
		for(size_t i=0; i < args.size(); i++)
			_args->AddField(args.at(i)->getValue(e, owner));
		return _args;
	}

}
