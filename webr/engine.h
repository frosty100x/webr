#ifndef WEBR_ENGINE
#define WEBR_ENGINE

#include <string.h>
#include <vector>
#include <list>
#include <deque>
#include <forward_list>
#include "jscontext.h"
#include "xparser.h"
#include "jitter.h"
#include "codegen.h"
//#include <boost/coroutine/all.hpp>

namespace webr {
	//class Parser;
	//class xJS::xParser;
	class Lexar;
	class Node;
	class JSValue;
	class JSString;
	class JSNumber;
	class JSContext;
	class JSArgs;
	class JSFunction;
	class JSUndefined;
	class JSBool;
	class JSReference;
	class Routine;
	class Jitter;
	typedef void (*Function)(JSArgs*);

	//namespace coo = boost::coroutines;

	//typedef coo::coroutine<JSValue*>::pull_type routine;
	//typedef coo::coroutine<void>::push_type co_push;
	//typedef coo::coroutine<JSValue*>::push_type caller;

	class Engine {
	public:

		std::string data;
		Engine();
		//~Engine();
		JSValue* Run(std::string text);
		JSString* NewString(std::string val);

		JSFunction* NewFunction(Function func);
		JSFunction* NewFunction(Function func, std::string name);
		JSNumber* NewNumber(double val);
		JSNumber* NaN();
		JSUndefined* Undefined();
		JSBool* NewBoolean(bool value);

		JSBool* True();
		JSBool* False();

		JSContext* Context();
		void PushRef(JSReference *ref);
		void GC();
		void Interrupt();
		void CreateRoutine(Node *exp);
		JSValue* StartCycle();
		int routs();
		bool ShouldHolt();
	private:
		char* opCodeBuff;
		int opCodePos;
		int gcCount;
		bool routineSwitch;
		std::list<JSValue*> objects;
		std::list<JSReference*> refs;
		std::vector<Node*> nodes;
		Internal::Lexar *lex;

		Internal::Jitter *vm;
		Internal::CodeBuff byteCode;

		Internal::xParser *parser;
		JSContext *cx;
		JSValue* Eval();
		int transferRoutines();

		//std::deque<Routine*> routines; //TODO: change to linked list;
		//std::deque<Routine*> backLog;

		bool shouldInterrupt;

	};


	class JSArgs {
		friend class CallExpr;
	public:
		JSArgs(Engine *e);
		int Length();
		JSValue* At(int s);

		void SetReturn(JSValue *val);
		JSValue* ReturnValue();
		Engine* GetEngine();

	private:
		std::vector<JSValue*> values;
		JSValue* return_value;
		void AddField(JSValue *val);
		Engine *e;
	};

}
#endif //WEBR_ENGINE
