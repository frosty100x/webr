
#include "jscontext.h"
#include "jsreference.h"
#include "engine.h"
#include <exception>
#include <iostream>
#include <deque>

namespace webr {
	JSContext::JSContext(Engine *e){
		JSContext(e, NULL, NULL);
	}
	JSContext::JSContext(Engine *e, JSContext *_parent){
		JSContext(e, _parent, globalCx());

	}

	JSContext::~JSContext(){}

	JSContext::JSContext(Engine *e, JSContext *_parent, JSContext *_global){
		this->e = e;
		this->parent = _parent;
		this->global = global;
		//refs = std::deque<JSReference*>();
	}

	JSReference* JSContext::exist(std::string name){
		for(size_t i=0; i<refs.size(); i++)
			if(refs[i]->Name() == name)
				return refs[i];
		return NULL;
	}

	JSReference* JSContext::globalExist(std::string name){
		JSReference *obj = exist(name);
		return (obj == NULL && !IsGlobal()) ? parent->Get(name) : obj;
	}

	void JSContext::GlobalSet(std::string name, JSValue *value){
		JSReference *obj = globalExist(name);

		if(obj != NULL){
			obj->Set(value);
		}else{
			if(!IsGlobal())
				global->Set(name, value);
			else{
				auto r = new JSReference(name, value);
				refs.push_back(r);
				//e->PushRef(r);
			}

		}
	}

	void JSContext::Set(std::string name, JSValue *value){
		JSReference *obj = exist(name);

		if(obj != NULL){
			obj->Set(value);
		}else{
			auto r = new JSReference(name, value);
			refs.push_back(r);
			//e->PushRef(r);
		}
	}

	JSReference* JSContext::Get(std::string name){
		JSReference *obj = exist(name);
		if(obj == NULL)
			return IsGlobal() ? NULL : parent->Get(name);
		return obj;
	}

	bool JSContext::IsGlobal(){
		return parent == NULL;
	}

	JSContext* JSContext::globalCx(){
		return IsGlobal() ? this : parent->globalCx();
	}

	Engine* JSContext::GetEngine(){
		return e;
	}

}
