#include "codegen.h"
#include "huballocator.h"
#include <cstdarg>
#include <array>
#include <iostream>
#include "xparser.h"

namespace webr {
	namespace Internal {
		using namespace Enumerations;
		CodeGen::CodeGen() {
			buff = new CodeBuff;
			buff->head = (intptr_t)HubAllocator::getSpace(100);
			buff->codeLength = 0;
			buff->buffSize = 0;
		}


		void CodeGen::setslot(slot dest, xNode *var) {
			switch (var->type) {
			case VARTYPE::CONSTANT_16BIT:
				setnum(dest, var->value);
				break;
			case VARTYPE::CONSTANT_BOOLEAN:
				setbool(dest, var->value);
				break;
			}
		}

		void CodeGen::setbool(slot dest, bool value) {
			putBytes(OpCode::SETBOOL);
			putBytes(dest);
			putBytes(value ? UnlinkedType::TRUE : UnlinkedType::FALSE);
		}

		//dest: destination (copy to) slot
		//source: source (copy from) slot
		void CodeGen::copyref(slot dest, slot source) {
			putBytes(OpCode::COPYR);
			putBytes(dest);
			putBytes(source);
		}
		//slot: slot to copy to
		//value: literal number to copy to (slot)
		void CodeGen::setnum(slot slot, intptr_t value) {
			putBytes(Enumerations::OpCode::SETNUM);
			putBytes(slot);
			putBytes(*(int16_t*)value);
		}

		void CodeGen::putnv(OpCode op, slot destSlot, intptr_t num, slot var) {
			putBytes(op);
			putBytes(destSlot);
			putBytes(*(int16_t*)num);
			putBytes(var);
		}

		void CodeGen::putvn(OpCode op, slot destSlot, slot var, intptr_t num) {
			putBytes(op);
			putBytes(destSlot);
			putBytes(var);
			putBytes(*(int16_t*)num);
		}

		void CodeGen::putvv(OpCode op, slot destSlot, slot slot1, slot slot2) {
			putBytes((char)op);
			putBytes(destSlot);
			putBytes(slot1);
			putBytes(slot2);
		}

		void CodeGen::setunf(slot s) {
			putBytes(OpCode::SETUN);
			putBytes(s);
		}

		CodeBuff* CodeGen::GetBuffer() {
			return buff;
		}

		template<typename T>
		void CodeGen::putBytes(T by) {
			*(T*)(buff->head + buff->codeLength) = by;
			buff->codeLength += sizeof(T);
		}

		void CodeGen::putInt16(int16_t value) {
			*((int16_t*)(buff->head + buff->codeLength)) = value;
			buff->codeLength += sizeof(int16_t);
		}
	}
}
