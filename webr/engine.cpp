#include <iostream>
#include "node.h"
#include "engine.h"
#include "lexar.h"
#include "jitter.h"
//#include "xparser.h"
#include "jsvalue.h"
#include "jsstring.h"
#include "jsnumber.h"
#include "jscontext.h"
#include "jsfunction.h"
#include "jsundefined.h"
#include "jsreference.h"
#include "jsbool.h"
#include "routine.h"
#include "const.h"
#include "errortypes.h"
#include "huballocator.h"

namespace webr {
	using namespace Internal;
	typedef JSValue* (Node::*ToJSValue)(Engine*);
	Engine::Engine() {
		//lex = new Lexar();
		parser = new Internal::xParser(50);
		//cx = new JSContext(this);
		//shouldInterrupt = true;
		//gcCount = 0;
		//routineSwitch = true;
		vm = new Internal::Jitter();
		//_global_proxys::factory = new FactoryAllocator();
	}


	JSValue* Engine::Eval(){
		//Block *ar = new Block(nodes);
		//CreateRoutine(ar);
		//return StartCycle();
		vm->Run((char*)byteCode.head);
		return new JSString("GREG");
	}


	JSValue* Engine::Run(std::string text){
		//lex->set(text);
		//std::vector<Node*> nodes;

		try{
			byteCode = *parser->parse(text);

/*			std::cout << "\nCode:[";
			for(int i=1; i < 50; i++)
				std::cout << byteCode.code[i] << ", ";
			std::cout << "]\n";*/
			return Eval();
			//return NULL;
		}catch(SyntaxError &e){
			std::cout << e.message() << std::endl;
		}catch(ReferenceError &e){
			std:: cout << e.message() << std::endl;
		}
		return NULL;
	}

	void Engine::Interrupt(){

		if(shouldInterrupt){
			//if(routineSwitch) yield();
			//gcCount = 0;
			//GC();
		}
	}

	bool Engine::ShouldHolt(){
		return true;
	}

	JSValue* Engine::StartCycle(){
		JSValue *last = nullptr;
//		for(size_t n=0; routines.size() > 0; n++){
//		//	if(!backLog.empty()) n += transferRoutines();
//
//			if(n == routines.size()) n = 0;
//			//Routine *rr = routines.at(n);
//			last = routines.at(n)->run(this);
//
//			if(last != NULL){
//				//std::cout << "Erasing: " << routines.begin().+n << std::endl;
//				routines.erase(routines.begin()+n);
//				n--;
//			}
//		}
		return last;

	}

/*	int Engine::transferRoutines(){
		int size = backLog.size();
		while(!backLog.empty()){
			//routines.push_front( backLog.back());
			backLog.pop_back();
		}
		return size;
	}*/

	/*int Engine::routs(){
		return routines.size();
	}*/

	void Engine::CreateRoutine(Node *exp){
		//Routine *rout = new Routine(exp, this);
		//routines.push_front(rout);
	}

	void Engine::PushRef(JSReference *ref) {
		//std::list<JSReference*> &gg = refs;
		if(ref != NULL)
			refs.push_back(ref);
	}

	void Engine::GC(){
		//auto objs = objects.begin();
		std::cout << objects.size() << std::endl << std::flush;
		bool found = false;
		for(auto _objs=objects.begin(); _objs != objects.end(); _objs++){
			found = false;
			for(auto _ref=refs.begin(); _ref != refs.end(); _ref++){
				if((*_objs) != (*_ref)->Value()) continue;
				else break;

				(*_objs)->Dispose();
				objects.erase(_objs);
			}
		}
	}

	JSContext* Engine::Context(){ return cx; }


	JSString* Engine::NewString(std::string value){
		JSString *ptr = new JSString(value);
		this->objects.push_back(ptr);
		return ptr;
	}

	JSNumber* Engine::NewNumber(double value){
		auto ptr = new JSNumber(value);
		this->objects.push_back(ptr);
		return ptr;
	}

	JSNumber* Engine::NaN(){
		auto ptr = new JSNumber();
		this->objects.push_back(ptr);
		return ptr;
	}

	JSUndefined* Engine::Undefined(){
		auto ptr = new JSUndefined();
		this->objects.push_back(ptr);
		return ptr;
	}

	//JSFunction* Engine::NewFunction(Function func){
	//	auto ptr = new JSFunction(func);
	//	this->objects.push_back(ptr);
	//	return ptr;
	//}

	//JSFunction* Engine::NewFunction(Function func, std::string name){
	//	auto ptr = new JSFunction(func, name);
	//	this->objects.push_back(ptr);
	//	return ptr;
	//}

	JSBool* Engine::NewBoolean(bool value){
		auto ptr = new JSBool(value);
		this->objects.push_back(ptr);
		return ptr;
	}

	JSBool* Engine::True(){
		return NewBoolean(true);
	}

	JSBool* Engine::False(){
		return NewBoolean(false);
	}

	//JSArgs-----------------------------

	JSArgs::JSArgs(Engine *e){
		this->e = e;
		return_value = nullptr;
	}

	int JSArgs::Length(){
		return values.size();
	}

	JSValue* JSArgs::At(int a){
		try{
			return values.at(a);
		}catch(std::exception &e){
			return NULL;
		}
	}

	void JSArgs::SetReturn(JSValue *val){
		return_value = val;
	}

	JSValue* JSArgs::ReturnValue(){
		return return_value == NULL ? e->Undefined() : return_value;
	}

	void JSArgs::AddField(JSValue *val){
		this->values.push_back(val);
	}

	Engine* JSArgs::GetEngine(){
		return e;
	}

}
