#ifndef WEBR_BOOLEAN
#define WEBR_BOOLEAN

#include "jsvalue.h"

namespace webr {
	class Engine;
	class JSBool : public JSValue{
	public:
		JSBool(bool value);
		~JSBool();
		JSValue* getValue(Engine *e, Routine *owner);
		std::string ToStringValue();
		bool ToBooleanValue();
		void Dispose();

	private:
		bool itr_value;
	};
}
#endif //WEBR_BOOLEAN
