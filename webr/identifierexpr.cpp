#include "engine.h"
#include "parser.h"
#include "jsreference.h"
#include "errortypes.h"
#include "jsvalue.h"
#include "jsobject.h"
#include "jsstring.h"
#include "jsundefined.h"

namespace webr {

	IdentifierExpr::IdentifierExpr(std::string name) : Expression(ExpressionType::IDENTIFIER){
		this->name = name;
	}

	std::string IdentifierExpr::getName(){
		return name;
	}

	JSReference* IdentifierExpr::getReference(Engine *e){
		return e->Context()->Get(name);
	}

	JSValue* IdentifierExpr::getValue(Engine *e, Routine *owner){
		JSReference *val = e->Context()->Get(name);
		if(val == NULL)
			throw ReferenceError(name + " is not defined", 1);
		else
			return val->Value();
	}
}
