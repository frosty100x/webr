
#include "parser.h"
#include "jsvalue.h"
#include "jsundefined.h"
#include "jsobject.h"
#include "errortypes.h"
namespace webr {
	ModifierExpr::ModifierExpr(Node *_base) : Expression(ExpressionType::MODIFIER) , base(_base){
	}

	JSValue* ModifierExpr::getValue(Engine *e, Routine *owner){
		this->e = e;
		//JSReference *val = e->Context()->Get(name);

		JSValue *current = base->getValue(e, owner);

		for(size_t n=0; n < modifiers.size(); n++){
			switch(modifiers.at(n).type){
			case 2: //member access;
				if(current->IsUndefined())
					//TODO: throw TypeError
					throw SyntaxError("TypeError");

				else if(current->IsObject()){
					auto ref = current->ToObject()->
							GetOwnProperty(modifiers.at(n).memberName);
					if(ref == NULL)
						//TODO: check object proto
						current = e->Undefined();
					else
						current = ref;
				}else{
					//TODO: check type proto
				}
			//case 1: //index
			}
		}

		return current;
	}

	void ModifierExpr::addMemberAccess(std::string name){
		modifiers.push_back(Modifier{2, 0, name});
	}


}
