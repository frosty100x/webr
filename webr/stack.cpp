#include "stack.h"
#include "huballocator.h"
#include "const.h"

namespace webr {
	namespace Internal {
		namespace Stack {

			namespace {
				//Enough for 50 slots
				static int16_t INIT_VAR_TABLE_SIZE = sizeof(intptr_t) * 50;

				static int16_t STACK_SIZE_OFFSET = 1;

				static int16_t SLOT_TABLE_OFFSET = STACK_SIZE_OFFSET + 4;

				static int16_t INIT_STACK_SIZE = 4096;
			}


			intptr_t getSlotPtr(intptr_t stackPtr, int16_t slot) {
				return ((intptr_t*)(stackPtr + SLOT_TABLE_OFFSET))[slot];
			}

			void push(intptr_t stackPtr, int16_t slot, intptr_t objPtr) {
				((intptr_t*)(stackPtr + SLOT_TABLE_OFFSET))[slot] = objPtr;
			}

			void* make() {
				char* ptr = (char*)HubAllocator::getSpace(INIT_STACK_SIZE);
				ptr[0] = (char)Enumerations::LinkedType::STACK;
				*((int16_t*)(ptr + STACK_SIZE_OFFSET)) = INIT_STACK_SIZE;
				return ptr;
			}
		}
	}
}
