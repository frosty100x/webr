#ifndef WEBR_JSVALUE
#define WEBR_JSVALUE

#include <string>
#include "node.h"
namespace webr {
	class Engine;
	class JSString;
	class JSNumber;
	class JSFunction;
	class JSBool;
	class JSObject;
	class JSValue : public Node {

	public:
		JSValue(int);
		virtual ~JSValue();
		bool IsString();
		bool IsNumber();
		bool IsFunction();
		bool IsObject();
		bool IsRegex();
		bool IsArray();
		bool IsUndefined();
		bool IsBoolean();

		int ValueType();

		Engine *getEngine();

		void operator+();

		JSString* ToString();
		JSNumber* ToNumber();
		JSFunction* ToFunction();
		JSBool* ToBoolean();
		JSObject* ToObject();

		virtual std::string ToStringValue() = 0;
		virtual bool ToBooleanValue() = 0;
		std::string operator<<(JSValue *a);
		virtual void Dispose() = 0;
	private:
		JSString* CastString();
		JSNumber* CastNumber();

	protected:
		int type;
	};
}
#endif
