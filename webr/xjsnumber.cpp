
#include "huballocator.h"
#include "xclass.h"
#include "const.h"

namespace webr {
	namespace Internal {
		namespace JSObjects {

			//		namespace {
			//			static int INIT_NUMBER_OBJECT_SIZE = 5;
			//			static int OBJECT_TYPE_OFFSET = 0;
			//			static int OBJECT_VALUE_OFFSET = OBJECT_TYPE_OFFSET+1;
			//			static int INIT_NUMBER_SIZE = 4; //32bit
			//		}

			namespace xJSNumber {

				intptr_t make(int32_t num) {
					intptr_t ptr = make();
					setValue(ptr, num);
					return ptr;
				}

				intptr_t make() {
					char *ptr = (char*)HubAllocator::getSpace(INIT_NUMBER_OBJECT_SIZE);
					ptr[OBJECT_TYPE_OFFSET] = (char)Enumerations::LinkedType::JSNUMBER;
					*((int32_t*)(ptr + OBJECT_VALUE_OFFSET)) = 0;
					return (intptr_t)ptr;
				}

				void setValue(intptr_t objectPtr, int32_t value) {
					*((int32_t*)(objectPtr + OBJECT_VALUE_OFFSET)) = value;
				}

				int32_t getValue(intptr_t objectPtr) {
					return *((int32_t*)(objectPtr + OBJECT_VALUE_OFFSET));
				}

			}
		}
	}
}
