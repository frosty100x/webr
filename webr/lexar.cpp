#include <iostream>
#include <vector>
#include <string>
#include "lexar.h"
#include "const.h"
namespace webr {
	namespace Internal {
		Lexar::Lexar() {
			this->pos = 0;
		}

		Lexar::~Lexar() {
			delete &data;
			delete &pos;
			delete this;
		}

		Lexar::Lexar(std::string data) {
			this->pos = 0;
			this->data = data;
			this->currentLine = 1;

		}

		std::vector<Token> Lexar::parse() {
			//while(c != 0) {
			for (char c = current(); (int)c != 0; c = next()) {
				if (isblank(c)) { continue; }
				//std::cout << "Checking:" << c << std::endl;
				if (isdigit(c)) readNumber();
				else if (isalpha(c)) readWord();
				else if (IsLineTerm(c)) readLineTerm();
				else if (IsQuote(c)) readString();
				else if (IsPunctuator(c)) readPunctuator();
				//else next();
			}
			Token to;
			to.type = TokenType::EOF_EOF;
			to.lineNumber = currentLine;
			tokens.push_back(to);
			return this->tokens;
		}

		void Lexar::set(std::string value) {
			data = value;
		}
		char Lexar::next() {
			return (pos < data.size()) ? data[++pos] : 0;
			//		if(pos < data.size())
			//			return data[++pos];
			//		return 0;
		}

		char Lexar::peek() {
			if (pos + 1 < data.size())
				return data[pos + 1];
			return 0;
		}
		char Lexar::current() {
			return data[pos];
		}

		char Lexar::back() {
			return data[pos - 1];
		}

		KeyWord Lexar::IsKeyword(std::string word) {
			if (word == "var")
				return KeyWord::VAR;
			if (word == "return")
				return KeyWord::RETURN;
			if (word == "function")
				return KeyWord::FUNCTION;
			if (word == "this")
				return KeyWord::THIS;
			if (word == "if")
				return KeyWord::IF;
			if (word == "else")
				return KeyWord::ELSE;
			if (word == "while")
				return KeyWord::WHILE;
			if (word == "for")
				return KeyWord::FOR;
			if (word == "continue")
				return KeyWord::CONTINUE;
			if (word == "routine")
				return KeyWord::ROUTINE;

			return KeyWord::UNKNOWN;
		}

		bool Lexar::IsQuote(char c) {
			return c == KeyCode::SINGLE_QUOTE || c == KeyCode::DOUBLE_QUOTE;
		}

		bool Lexar::IsPunctuator(char c) {
			switch (c) {
			case KeyCode::OPEN_CURLY_BRACKET: case KeyCode::CLOSE_CURLY_BRACKET: case KeyCode::OPEN_PARENTHESES: case KeyCode::CLOSE_PERENTHESES:
			case KeyCode::OPEN_BRACKET: case KeyCode::CLOSE_BRACKET: case KeyCode::SEMICOLON: case KeyCode::ASTERISK: case KeyCode::GREATER_THAN:
			case KeyCode::LESS_THAN: case KeyCode::EQUALS: case KeyCode::QUESTION_MARK: case KeyCode::COLON: case KeyCode::MINUS: case KeyCode::PLUS:
			case KeyCode::EXCLAMATION: case KeyCode::AMPERSAND: case KeyCode::GRAVE_ACCENT: case KeyCode::PERCENT: case KeyCode::PERIOD:
			case KeyCode::COMMA: case KeyCode::VERTICAL_BAR: case KeyCode::Tilde: case KeyCode::SLASH: case KeyCode::BACK_SLASH:
				return true;
			default:
				return false;
			}
		}

		bool Lexar::IsLineTerm(char c) {
			return c == 10;
		}

		Punctuator Lexar::readEqualPostfix() {
			char c = peek();
			switch (c) {
			case KeyCode::EQUALS:
				next();
				if (peek() == KeyCode::EQUALS) {
					//next();
					return Punctuator::PERCISION_EQUAL_TO;
				}
				else {
					//next();
					return Punctuator::EQUAL_TO;
				}
				/*case KeyCode::GREATER_THAN:
					next();
					return Punctuator::FAT_ARROW;*/
			default:
				return Punctuator::NONE;
			}
		}

		Punctuator Lexar::readLTPostfix() {
			char c = peek();
			switch (c) {
			case KeyCode::EQUALS:
				next();
				return Punctuator::LT_EQUAL;
			case KeyCode::LESS_THAN:
				next();
				if (peek() == KeyCode::EQUALS) {
					//next();
					return Punctuator::SHIFT_LEFT_ASSIGN;
				}
				else {
					//next();
					return Punctuator::SHIFT_LEFT;
				}
			default:
				return Punctuator::NONE;
			}

		}

		Punctuator Lexar::readGTPostfix() {
			char c = peek();
			switch (c) {
			case KeyCode::GREATER_THAN:
				next();
				if (peek() == KeyCode::GREATER_THAN) {
					next();
					if (peek() == KeyCode::EQUALS) {
						//next();
						return Punctuator::SHIFT_RIGHT_UNSIGNED_ASSIGN;
					}
					else {
						//next();
						return Punctuator::SHIFT_RIGHT_UNSIGNED;
					}
				}
				else if (peek() == KeyCode::EQUALS) {
					//next();
					return Punctuator::SHIFT_RIGHT_ASSIGN;
				}
				next();
				return Punctuator::SHIFT_RIGHT;
			case KeyCode::EQUALS:
				next();
				return Punctuator::GT_EQUAL;
			default:
				return Punctuator::NONE;
			}
		}

		Punctuator Lexar::readAmpPostfix() {
			char c = peek();
			switch (c) {
			case KeyCode::AMPERSAND:
				next();
				return Punctuator::AND;
			case KeyCode::EQUALS:
				next();
				return Punctuator::BITWISE_AND_ASSIGN;
			default:
				return Punctuator::NONE;
			}
		}

		Punctuator Lexar::readVertBarPostfix() {
			char c = peek();
			switch (c) {
			case KeyCode::VERTICAL_BAR:
				next();
				return Punctuator::OR;
			case KeyCode::EQUALS:
				next();
				return Punctuator::BITWISE_OR_ASSIGN;
			default:
				return Punctuator::NONE;
			}
		}

		Punctuator Lexar::readExclPostfix() {
			char c = peek();
			switch (c) {
			case KeyCode::EQUALS:
				next();
				if (peek() == KeyCode::EQUALS) {
					next();
					return Punctuator::PERCISION_NOT_EQUAL_TO;
				}
				else {
					next();
					return Punctuator::NOT_EQUAL_TO;
				}
			default:
				return Punctuator::NONE;
			}
		}

		Punctuator Lexar::readPlusPostfix() {
			char c = peek();

			switch (c) {
			case KeyCode::PLUS:
				next();
				return Punctuator::INCREMENTER;
			case KeyCode::EQUALS:
				next();
				return Punctuator::ADD_ASSIGN;
			default:
				return Punctuator::NONE;
			}
		}

		Punctuator Lexar::readMinusPostfix() {
			char c = peek();

			switch (c) {
			case KeyCode::MINUS:
				next();
				return Punctuator::DEINCREMENTER;
			case KeyCode::EQUALS:
				next();
				return Punctuator::MINUS_ASSIGN;
			default:
				return Punctuator::NONE;
			}
		}

		void Lexar::readPunctuator() {
			std::string str; str += current();

			Token to;
			to.type = TokenType::PUNCUATOR;

			if (str == ".") {
				if (isdigit(peek())) {
					readNumber();
					return;
				}
				else {
					to.punc = Punctuator::PROPERTY_ACCESSOR;
				}
			}
			else if (str == ";")
				to.punc = Punctuator::STATMENT_TERMINATOR;
			else if (str == ">") {
				Punctuator type = readGTPostfix();
				to.punc = (type != Punctuator::NONE) ? type : Punctuator::GREATER_THAN;
			}
			else if (str == "<") {
				Punctuator type = readLTPostfix();
				to.punc = (type != Punctuator::NONE) ? type : Punctuator::LESS_THAN;
			}
			else if (str == "&") {
				Punctuator type = readAmpPostfix();
				to.punc = (type != Punctuator::NONE) ? type : Punctuator::BITWISE_AND;
			}
			else if (str == "|") {
				Punctuator type = readVertBarPostfix();
				to.punc = (type != Punctuator::NONE) ? type : Punctuator::BITWISE_OR;
			}
			else if (str == "!") {
				Punctuator type = readExclPostfix();
				to.punc = (type != Punctuator::NONE) ? type : Punctuator::NOT;
			}
			else if (str == ",")
				to.punc = Punctuator::ARGUMENT_DELIMITER;
			else if (str == "{")
				to.punc = Punctuator::BlOCK_START;
			else if (str == "}")
				to.punc = Punctuator::BLOCK_END;
			else if (str == "(")
				to.punc = Punctuator::OPEN_PARENTHESES;
			else if (str == ")")
				to.punc = Punctuator::CLOSE_PERENTHESES;
			else if (str == "[")
				to.punc = Punctuator::OPEN_INDEX_DELIMITER;
			else if (str == "]")
				to.punc = Punctuator::CLOSE_INDEX_DELIMITER;
			else if (str == ":")
				to.punc = Punctuator::PART_CONDITIONAL_OPERATOR;
			else if (str == "+") {
				Punctuator type = readPlusPostfix();
				to.punc = (type != Punctuator::NONE) ? type : Punctuator::PLUS;
			}
			else if (str == "-") {
				Punctuator type = readMinusPostfix();
				to.punc = (type != Punctuator::NONE) ? type : Punctuator::MINUS;
			}
			else if (str == "/") {
				if (peek() == KeyCode::EQUALS) {
					next();
					to.punc = Punctuator::DIVIDE_ASSIGN;
				}
				else if (peek() == KeyCode::SLASH) {
					readComment(); return;
				}
				else to.punc = Punctuator::DIVIDE;
			}
			else if (str == "*") {
				if (peek() == KeyCode::EQUALS) {
					next();
					to.punc = Punctuator::MULTIPLY_ASSIGN;
				}
				else to.punc = Punctuator::MULTIPLY;
			}
			else if (str == "=") {
				Punctuator type = readEqualPostfix();
				to.punc = (type != Punctuator::NONE) ? type : Punctuator::ASSIGN;
			}
			else if (str == "%") {
				if (peek() == KeyCode::EQUALS) {
					next();
					to.punc = Punctuator::REMAINDER_ASSIGN;
				}
				else to.punc = Punctuator::REMAINDER;
			}
			else {
				std::cout << "Unrecognized token: " << str << std::endl;
				throw "Unrecognized token: " + str;
			}

			to.literalValue = FromPunctuator(to.punc);
			to.lineNumber = currentLine;
			tokens.push_back(to);

		}

		void Lexar::readComment() {
			next();
			while (peek() != '\n' && peek() != 0) next();
		}

		char Lexar::readEscape() {
			char c = peek();
			if (c == 'n') { next(); return '\n'; }
			else if (c == 'r') { next(); return '\r'; }
			return current();
		}

		void Lexar::readString() {
			char start = current();
			std::string str;
			while (peek() != 0 && (peek() != start || current() == KeyCode::BACK_SLASH)) {
				if (peek() == KeyCode::BACK_SLASH) { next(); str += readEscape(); }
				else str += next();
			}

			//if(current() != start)
				//throw unexpected EOF
			next(); //skip quote char
			Token to;
			to.type = TokenType::STRING_LITERAL;
			to.literalValue = str;
			to.lineNumber = currentLine;
			tokens.push_back(to);
		}

		void Lexar::readLineTerm() {
			currentLine++;
			Token to;
			to.type = TokenType::LINE_TERMINATOR;
			to.lineNumber = currentLine;
			//to.literalValue = current();
			//store operator type;
			tokens.push_back(to);
		}

		void Lexar::readWord() {
			Token to;
			std::string str; str += current();

			while (isalnum(peek())) str += next();

			KeyWord word = IsKeyword(str);
			if (word != KeyWord::UNKNOWN) {
				to.type = TokenType::KEYWORD;
				to.word = word;
			}
			else if (str == "true") {
				to.type = TokenType::BOOLEAN_LITERAL;
				to.boolValue = true;
			}
			else if (str == "false") {
				to.type = TokenType::BOOLEAN_LITERAL;
				to.boolValue = false;
			}
			else if (str == "null") {
				to.type = TokenType::NULL_LITERAL;
			}
			else if (str == "undefined") {
				to.type = TokenType::UNDEFINED_LITERAL;
			}
			else {
				to.type = TokenType::IDENTIFIER;
				to.literalValue = str;
			}

			to.lineNumber = currentLine;
			tokens.push_back(to);
		}

		void Lexar::readNumber() {
			std::string number; number += current();

			while (isdigit(peek()))
				number += next();

			if (peek() == KeyCode::PERIOD) {
				number += next();
				while (isdigit(peek()))
					number += next();
			}

			Token to;
			to.type = TokenType::NUMBER_LITERAL;
			to.literalValue = number;
			to.lineNumber = currentLine;
			tokens.push_back(to);
		}

		void Lexar::reset(int pos) {
			this->pos = pos;
		}

		void error(const char *err) {
			throw err;
		}
	}
}
