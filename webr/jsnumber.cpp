
#include "jsnumber.h"
#include "const.h"
#include "util.h"
namespace webr {
	using namespace Internal::Enumerations;
	JSNumber::JSNumber(double _value) : JSValue(VariableType::NUMBER){
		ptr_value = new double(_value);
		isNaN = false;
	}

	JSNumber::JSNumber() : JSValue(VariableType::NUMBER){
		isNaN = true;
	}

	double JSNumber::Value(){
		return IsNaN() ? 0 : *ptr_value;
	}

	JSValue* JSNumber::getValue(Engine *e, Routine *owner){
		return this;
	}

	std::string JSNumber::ToStringValue(){
		return isNaN ? "NaN" : util::ntos(*ptr_value);
	}

	bool JSNumber::ToBooleanValue(){
		return *ptr_value == 0 ? false : true;
	}

	bool JSNumber::IsNaN(){
		return isNaN;
	}

	void JSNumber::MakePositive(){
		if(!isNaN) *ptr_value = -*ptr_value;
	}

	void JSNumber::MakeNegative(){
		if(!isNaN) *ptr_value = +*ptr_value;
	}

	void JSNumber::Increment(){
		if(!isNaN) *ptr_value++;
	}

	void JSNumber::Deincrement(){
		if(!isNaN) *ptr_value--;
	}

	void JSNumber::Dispose(){
		delete ptr_value;
		delete this;
	}

}
