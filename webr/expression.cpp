
#ifndef WEBR_EXPRESSION
#define WEBR_EXPRESSION

#include "parser.h"

namespace webr {
	class JSValue;
	Expression::Expression(int type) : Node(NodeType::EXPRESSION) {
		exprtype = type;
	}

	Expression::~Expression(){}

	int Expression::ExprType(){
		return exprtype;
	}

	bool Expression::IsIdentifier(){
		return exprtype == ExpressionType::IDENTIFIER;
	}

	bool Expression::IsModifier(){
		return exprtype == ExpressionType::MODIFIER;
	}

	IdentifierExpr* Expression::ToIdentifier(){
		return IsIdentifier() ? (IdentifierExpr*)this : NULL;
	}

}

#endif
