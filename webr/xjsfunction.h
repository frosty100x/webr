#ifndef WEBR_XJSFUNCTION
#define WEBR_XJSFUNCTION

namespace webr {

	namespace JSObjects {

		namespace xJSFunction {

			static char* callableBlock(char* ptr);

		}
	}
}
#endif //WEBR_XJSFUNCTION
