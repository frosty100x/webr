
#ifndef WEBR_JSNUMBER
#define WEBR_JSNUMBER

#include "jsvalue.h"
namespace webr{
	class Parser;
	class JSNumber : public JSValue {
		friend Engine; friend Parser;
	public:

		double Value();

		JSValue* getValue(Engine *e, Routine *owner);

		std::string ToStringValue();
		bool ToBooleanValue();
		bool IsNaN();

		void MakePositive();
		void MakeNegative();

		void Increment();
		void Deincrement();

		void Dispose();
	private:
		JSNumber();
		JSNumber(double val);

		double *ptr_value;
		bool isNaN;
	};
}
#endif
