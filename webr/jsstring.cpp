#include <iostream>
#include "jsstring.h"
#include "const.h"

namespace webr {
	using namespace Internal::Enumerations;
	JSString::JSString(std::string _value) : JSValue(VariableType::STRING) {
		ptr_value = new std::string(_value);
	}

	std::string JSString::str(){
		return *ptr_value;
	}

	int JSString::length(){
		return ptr_value->length();
	}

	JSValue* JSString::getValue(Engine *e, Routine *owner){
		return this;
	}

	std::string JSString::ToStringValue(){
		return str();
	}

	bool JSString::ToBooleanValue(){
		return ptr_value->size() == 0 ? false : true;
	}

	void JSString::Dispose(){
		delete ptr_value;
		delete this;
	}

}
