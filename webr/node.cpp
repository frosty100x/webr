
#include "node.h"
#include "parser.h"
#include "const.h"
//#include <boost/coroutine/all.hpp>
namespace webr {
	using namespace Internal::Enumerations;
	Node::Node(int _type){
		nodetype = _type;
	}

	Node::~Node(){}

	JSValue* Node::ToJSValue(){
		return IsJSValue() ? (JSValue*)this : 0;
	}

	Expression* Node::ToExpression(){
		return IsExpression() ? (Expression*)this : 0;
	}

	APunctuator* Node::ToAPunctuator(){
		return IsAPunctuator() ? (APunctuator*)this : 0;
	}

	JSReference* Node::ToReference(){
		return IsReference() ? (JSReference*)this : 0;
	}

	int Node::NodeType(){
		return this->nodetype;
	}

	bool Node::IsExpression(){
		return (this->nodetype == NodeType::EXPRESSION) ? true : false;
	}

	bool Node::IsJSValue(){
		return (this->nodetype == NodeType::VALUE) ? true : false;
	}

	bool Node::IsAPunctuator(){
		return (this->nodetype == NodeType::APUNCTUATOR) ? true : false;
	}

	bool Node::IsReference(){
		return (this->nodetype == NodeType::REFERENCE) ? true : false;
	}


	/*
	JSBinaryExpr* Node::ToBinaryExpr(){
	(this->Type() == NodeType::EXP_BINARY) ?
		static_cast<JSValue*>(this) : NULL;
}
*/

}
