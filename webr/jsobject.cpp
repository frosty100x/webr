#include "const.h"
#include "engine.h"
#include "jsstring.h"
#include "jsobject.h"


namespace webr {
	using namespace Internal::Enumerations;
	JSObject::JSObject() : JSValue(VariableType::OBJECT), isEvaluated(false){}

	void JSObject::addField(std::string key, Node* value){
		_objs[key] = value;
	}

	JSValue* JSObject::getValue(Engine *e, Routine *owner){

		if(!isEvaluated){
			for(std::map<std::string, Node*>::iterator it=_objs.begin(); it != _objs.end(); ++it){
				_objs[it->first] = it->second->getValue(e, owner);
			}
			isEvaluated = true;
		}
		return this;
	}

	std::string JSObject::ToStringValue(){
		return "[object Object]";
	}

	bool JSObject::ToBooleanValue(){
		return !(this == nullptr);
	}

	JSValue* JSObject::GetOwnProperty(std::string name){
		try{
			return _objs.at(name)->ToJSValue();
		}catch(std::exception &e){
			return NULL;
		}
	}

	void JSObject::Dispose(){}

}
