#include "parser.h"
#include "const.h"
#include "engine.h"
#include "jsundefined.h"
#include <vector>

namespace webr {

	Block::Block(std::vector<Node*> _nodes) : Node(NodeType::BLOCK), nodes(_nodes){}

	JSValue* Block::getValue(Engine *e, Routine *owner){
		JSValue *last = nullptr;
		for(size_t i=0; i < nodes.size(); i++){
			Node *rr = nodes.at(i);
			last = nodes.at(i)->getValue(e, owner);
		}
		return (last == NULL) ? e->Undefined() : last;
	}

}
