#ifndef WEBR_JSOBJECT
#define WEBR_JSOBJECT

#include "jsvalue.h"
#include <string>
#include <map>

namespace webr {

	class JSObject : JSValue {
	friend class Parser;
	friend class Engine;
	public:


		void addField(std::string key, Node* value);

		JSValue* getValue(Engine *e, Routine *owner);

		JSValue* GetOwnProperty(std::string name);
		std::string ToStringValue();
		bool ToBooleanValue();

		void Dispose();

	private:
		bool isEvaluated;
		JSObject();
		std::map<std::string, Node*> _objs;

	};

}

#endif //WEBR_JSOBJECT
