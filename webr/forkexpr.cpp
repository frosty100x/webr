
#include "node.h"
#include "parser.h"
#include "routine.h"
#include "engine.h"
#include "jsundefined.h"
namespace webr {


	ForkExpr::ForkExpr(Node *_exp): Node(ExpressionType::FORK), exp(_exp) {}

	JSValue* ForkExpr::getValue(Engine *e, Routine *owner){
		e->CreateRoutine(exp);
		owner->holt();
		return e->Undefined();
	}


}
