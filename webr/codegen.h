#ifndef WEBR_CODEGEN
#define WEBR_CODEGEN

#include "const.h"
#include "xclass.h"

namespace webr {
	namespace Internal {
		struct xNode;

		struct CodeBuff {
			intptr_t head;
			int buffSize;
			int codeLength;
		};

		class CodeGen {
		public:
			CodeGen();
			void setslot(slot dest, xNode *var);
			void setbool(slot dest, bool value);
			void copyref(slot dest, slot source);
			void setnum(slot slot, intptr_t value);
			void setunf(slot slot);
			void putvn(Enumerations::OpCode op, slot destSlot, slot var, intptr_t lit);
			void putvv(Enumerations::OpCode op, slot destSlot, slot slot1, slot slot2);
			void putnv(Enumerations::OpCode op, slot destSlot, intptr_t lit, slot var);

		CodeBuff* GetBuffer();

		private:
			template<typename T>
			void putBytes(T byte);
			void putInt16(int16_t value);
			CodeBuff *buff;
		};
	}
}

#endif // WEBR_CODEGEN
