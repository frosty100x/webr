
#include "linkedlist.h"
namespace webr {
	namespace util {

		template<class T>
		LinkedList<T>::LinkedList(int s){
			head = nullptr;
			tail = head;
			_size = 0;
		}

		template<class T>
		void LinkedList<T>::push_back(T el){
			auto nel = new _linked_item;
			nel->data = new T(el);
			nel->next = nullptr;
			tail->next = nel;
			tail = nel;
			_size++;
		}

		template<class T> int LinkedList<T>::size(){
			return _size;
		}

		template<class T>T LinkedList<T>::back(){
			return (T)tail->data;
		}

		template<class T>T LinkedList<T>::front(){
			return (T)head->data;
		}

		template<class T>
		_iterator* LinkedList<T>::begin(){
			return _iterator(head);
		}

	}



}
