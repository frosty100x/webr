#include "jsfunction.h"
#include "engine.h"
#include "const.h"
#include "jsstring.h"
#include "jsundefined.h"

namespace webr{
	using namespace Internal::Enumerations;
	JSFunction::JSFunction(Function func, std::string name) : JSValue(VariableType::FUNCTION) {
		native_func = func;
		func_name = name;
		IsNative = true;
	}

	JSFunction::JSFunction(Function func) : JSValue(VariableType::FUNCTION) {
		native_func = func;
		IsNative = true;
	}

	JSValue* JSFunction::getValue(Engine *e, Routine *owner){
		if(IsNative)
			return e->NewString("function "+ func_name + "() { [native code] }")->ToJSValue();
		else{
			return NULL;
			//TODO: return function source text.
		}
	}

	std::string JSFunction::ToStringValue(){
		return "function "+ func_name + "() { [native code] }";
	}

	bool JSFunction::ToBooleanValue() {
		return true;
	}

	JSValue* JSFunction::Call(JSArgs *args, Engine *e){
		if(IsNative){
			native_func(args);
			return args->ReturnValue();
		}else{
			//TODO: Call non-native method
			return NULL;
		}
	}

	void JSFunction::Dispose(){
		delete &func_name;
		delete this;
	}



}
