#include "jsundefined.h"
#include "const.h"
#include <string>
namespace webr {
	using namespace Internal::Enumerations;
	JSUndefined::JSUndefined() : JSValue(VariableType::UNDEFINED){}

	JSValue* JSUndefined::getValue(Engine *e, Routine *owner){ return this; }

	std::string JSUndefined::ToStringValue(){ return "undefined"; }

	bool JSUndefined::ToBooleanValue(){ return false; }

	void JSUndefined::Dispose(){
		delete this;
	}

}
