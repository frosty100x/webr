
//============================================================================
// Name        : main.cpp
// Author      : Greg Batie
// Version     :
// Copyright   : Your copyright notice
// Description : webr javascript engine.
//============================================================================
#define __STDC_LIMIT_MACROS 100
#define DASM_VERSION 10300
#include <string>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <cctype>
#include <cstdio>
#include <map>
#include "webr.h"
#include <time.h>
//#include "linkedlist.h"
//#include "jsfactory.h"

#include "jitter.h"

using namespace std;

webr::Engine *en;

void print(webr::JSArgs *args){
	for(int i=0; i<args->Length(); i++) {
		webr::JSValue *rr = args->At(i);
		std::string ss = rr->ToStringValue();

		cout << args->At(i)->ToStringValue() << flush;
	}
}

void readFile(string name, string& content){
	std::ifstream t(name);
	content.append((std::istreambuf_iterator<char>(t)),
	                 std::istreambuf_iterator<char>());
}


int main(int argc, char* args[]) {
	clock_t start, finish;
	start = clock();
	en = new webr::Engine();
	string content;
	readFile("C:\\Users\\gbatie\\Desktop\\repos\\webr\\webr\\js\\test.js", content);

	webr::JSValue *value = en->Run(content);
	finish = clock();

	cout << "Done: " << start << " " << finish << " " << ((double)(finish - start) / CLOCKS_PER_SEC) << endl;
	return 0;
}
