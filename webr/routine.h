#ifndef _WEBR_ROUTINE_
#define _WEBR_ROUTINE_

//#include <boost/context/all.hpp>

namespace webr {
	//namespace ctx = boost::context;
	class Node;
	class Engine;
	class JSValue;
	class Routine {
	public:
		Routine(Node * _exp, Engine *e);
		JSValue* run(Engine *e);
		void holt();
		void entry();
		Engine *getEngine();
		Node* getExp();
	private:
		//std::size_t stack_size;
		Node *exp;
		Engine *e;
		JSValue *return_value;
		//ctx::fcontext_t last_pos;
		//ctx::fcontext_t origin_call;
		bool isStarted;
		bool isFinished;
		void *origin_stack;
		void *stack1;
		void *stack_start;
		void makeContext();
		int resume();
		void yield(bool);
	};
}

#endif //_WEBR_ROUTINE_
