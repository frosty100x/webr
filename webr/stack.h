#ifndef WEBR_STACK
#define WEBR_STACK

#include "stddef.h"
#include "sys/types.h"
#include <cstdint>
namespace webr {

	namespace Internal {
		namespace Stack {

			void* make();

			intptr_t getSlotPtr(intptr_t ptr, int16_t slot);
			void* getSlot(unsigned char a, unsigned char b);

			void push(intptr_t stackPtr, int16_t slot, intptr_t pointer);

		}
	}
}

#endif // WEB_STACK
