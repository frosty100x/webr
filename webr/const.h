#ifndef WEBR_CONST
#define WEBR_CONST
#include <string>
namespace webr {

	namespace Internal {
		namespace Enumerations {

			enum class VARTYPE {
				CONSTANT_8BIT, CONSTANT_16BIT, CONSTANT_32BIT,
				CONSTANT_64BIT, CONSTANT_STRING, SLOT_NUMBER,
				CONSTANT_BOOLEAN,
				PUNCUATOR //special type for binary operations parsing
			};

			class KeyCode {
			public:
				static const int
					N0 = 48, N1 = 49, N2 = 50, N3 = 51, N4 = 52, N5 = 53, N6 = 54, N7 = 55, N8 = 56, N9 = 57,
					A = 65, B = 66, C = 67, D = 68, E = 69, F = 70, G = 71, H = 72, I = 73, J = 74,
					K = 75, L = 76, M = 77, N = 78, O = 79, P = 80, Q = 81, R = 82, S = 83, T = 84,
					U = 85, V = 86, W = 87, X = 88, Y = 89, Z = 90,

					EXCLAMATION = 33, DOUBLE_QUOTE = 34, POUND = 35, DOLLAR_SIGN = 36, PERCENT = 37, AMPERSAND = 38,
					SINGLE_QUOTE = 39, OPEN_PARENTHESES = 40, CLOSE_PERENTHESES = 41, ASTERISK = 42, PLUS = 43, COMMA = 44,
					MINUS = 45, PERIOD = 46, SLASH = 47, COLON = 58, SEMICOLON = 59, LESS_THAN = 60, EQUALS = 61, GREATER_THAN = 62,
					QUESTION_MARK = 63, AT = 64, OPEN_BRACKET = 91, BACK_SLASH = 92, CLOSE_BRACKET = 93, CIRCUMFLEX = 94, UNDERSCORE = 95,
					GRAVE_ACCENT = 96, OPEN_CURLY_BRACKET = 123, VERTICAL_BAR = 124, CLOSE_CURLY_BRACKET = 125, Tilde = 126;
			};

			enum class KeyWord : int {
				BREAK = 201, CASE = 202, CATCH = 203, /*CLASS = 204, CONST = 205,*/ CONTINUE = 206, /*DEBUGGER = 207,*/
				DEFAULT = 208, DELETE = 209, DO = 210, ELSE = 211, /*EXPORT = 212, EXTENDS = 213,*/ FINALLY = 214,
				FOR = 215, FUNCTION = 216, IF = 217, /*IMPORT = 218,*/ IN = 219, INSTANCEOF = 220, NEW = 221, RETURN = 222,
				/*SUPER = 223,*/ SWITCH = 224, THIS = 225, THROW = 226, TRY = 227, TYPEOF = 228, VAR = 229, VOID = 230,
				WHILE = 231, WITH = 232, /*YIELD = 233, ENUM = 234, */ ROUTINE = 233,

				UNKNOWN = 0
			};

			static std::string FromKeyWord(KeyWord word) {
				switch (word) {
				case KeyWord::IF: return "if";
				case KeyWord::WHILE: return "while";
				case KeyWord::ELSE: return "else";
				case KeyWord::FUNCTION: return "function";
				case KeyWord::VAR: return "var";
				case KeyWord::ROUTINE: return "routine";
				default: return "unrecognized word";
				}
			}


			enum class Punctuator : int {
				STATMENT_TERMINATOR = 301, OPEN_PARENTHESES = 302, CLOSE_PERENTHESES = 303, BlOCK_START = 304, BLOCK_END = 305,
				OPEN_INDEX_DELIMITER = 306, CLOSE_INDEX_DELIMITER = 307, PROPERTY_ACCESSOR = 308, /*ELLIPSIS = 309,*/ AND = 310,
				ARGUMENT_DELIMITER = 311, GREATER_THAN = 312, LESS_THAN = 313, LT_EQUAL = 314, GT_EQUAL = 315, EQUAL_TO = 316,
				NOT_EQUAL_TO = 317, PERCISION_EQUAL_TO = 318, PERCISION_NOT_EQUAL_TO = 319, PLUS = 320, MINUS = 321, MULTIPLY = 322,
				REMAINDER = 323, INCREMENTER = 324, DEINCREMENTER = 325, SHIFT_RIGHT = 326, SHIFT_LEFT = 327, SHIFT_RIGHT_UNSIGNED = 328,
				BITWISE_AND = 329, BITWISE_OR = 330, OR = 331, CONDITIONAL_OPERATOR = 332, PART_CONDITIONAL_OPERATOR = 333, ASSIGN = 334,
				ADD_ASSIGN = 335, MINUS_ASSIGN = 336, MULTIPLY_ASSIGN = 337, REMAINDER_ASSIGN = 338, SHIFT_LEFT_ASSIGN = 339,
				SHIFT_RIGHT_ASSIGN = 340, SHIFT_RIGHT_UNSIGNED_ASSIGN = 341, BITWISE_AND_ASSIGN = 342, BITWISE_OR_ASSIGN = 343,
				BITWISE_XOR = 344, BITWISE_XOR_ASSIGN = 345, /* FAT_ARROW  = 346,*/ DIVIDE = 347, DIVIDE_ASSIGN = 348, NOT = 349,

				NONE = 0
			};
			static std::string FromPunctuator(Punctuator value) {
				switch (value) {
				case Punctuator::PLUS: return "+";
				case Punctuator::MINUS: return "-";
				case Punctuator::DIVIDE: return "/";
				case Punctuator::MULTIPLY: return "*";
				case Punctuator::ASSIGN: return "=";
				case Punctuator::OPEN_PARENTHESES: return "(";
				case Punctuator::CLOSE_PERENTHESES: return ")";
				case Punctuator::REMAINDER: return "%";
				case Punctuator::REMAINDER_ASSIGN: return "%=";
				case Punctuator::ADD_ASSIGN: return "+=";
				case Punctuator::DIVIDE_ASSIGN: return "/=";
				case Punctuator::EQUAL_TO: return "==";
				case Punctuator::BlOCK_START: return "{";
				case Punctuator::BLOCK_END: return "}";
				case Punctuator::GREATER_THAN: return ">";
				case Punctuator::LESS_THAN: return "<";
				case Punctuator::PROPERTY_ACCESSOR: return ".";
				case Punctuator::GT_EQUAL: return ">=";
				case Punctuator::LT_EQUAL: return "<=";
				case Punctuator::OPEN_INDEX_DELIMITER: return "[";
				case Punctuator::CLOSE_INDEX_DELIMITER: return "]";
				case Punctuator::INCREMENTER: return "++";
				case Punctuator::DEINCREMENTER: return "--";
				case Punctuator::NOT: return "!";
				case Punctuator::STATMENT_TERMINATOR: return ";";

				default: return "unrecognized";
				}
			}


			enum class TokenType : int {
				NUMBER_LITERAL = 101,
				STRING_LITERAL = 102,
				BOOLEAN_LITERAL = 109,
				REGULAR_EXPRESSION_LITERAL = 103,
				KEYWORD = 104,
				PUNCUATOR = 105,
				LINE_TERMINATOR = 106,
				IDENTIFIER = 107,
				UNDEFINED_LITERAL = 110,
				NULL_LITERAL = 111,
				EOF_EOF = 108,
				NONE = 0
			};
			static std::string FromTokenType(TokenType token) {
				switch (token) {
				case TokenType::NUMBER_LITERAL: return "NUMBER_LITERAL";
				case TokenType::PUNCUATOR: return "PUNCUATOR";
				case TokenType::STRING_LITERAL: return "STRING_LITERAL";
				case TokenType::EOF_EOF: return "EOF_EOF";
				case TokenType::LINE_TERMINATOR: return "LINE_TERMINATOR";
				case TokenType::IDENTIFIER: return "IDENTIFIER";
				case TokenType::KEYWORD: return "KEYWORD";
				case TokenType::REGULAR_EXPRESSION_LITERAL: return "REGULAR_EXPRESSION_LITERAL";
				case TokenType::BOOLEAN_LITERAL: return "BOOLEAN_LITERAL";
				case TokenType::NONE: return "NONE";

				default: return "UNKNOWN";

				}
			};

			class VariableType {
			public:
				static const int STRING = 501, OBJECT = 502, NUMBER = 503, FUNCTION = 504,
					UNDEFINED = 505, REGEX = 506, NULL_NULL = 507, ARRAY = 508, BOOLEAN = 509;
			};

			class NodeType {
			public:
				static const int
					VALUE = 601, EXPRESSION = 602, APUNCTUATOR = 603, REFERENCE = 604, WHILE_STATEMENT = 605,
					IF_STATEMENT = 608, BLOCK = 609;
				//EXP_BINARY = 601, EXP_FUNCTION_CALL = 602, FUNCTION_DEF = 603, FOR_LOOP = 604,
				//, NUMBER_LITERIAL = 607, STRING_LITERIAL = 608,
				//NUMBER_VARIABLE = 610;

				static std::string From(int value) {
					switch (value) {
					case NodeType::VALUE: return "VALUE";
					case NodeType::EXPRESSION: return "EXPRESSION";
					case NodeType::APUNCTUATOR: return "APUNCTUATOR";
					default: return "UNKNOWN";
					}
				}
			};

			class ExpressionType {
			public:
				static const int
					BINARY = 801, FUNCTION_CALL = 802, IDENTIFIER = 803, MODIFIER = 804, FORK = 805,
					ASSIGNMENT = 806;
			};

			enum class OpCode : char {
				ADDVN = 1, SUBVN = 2, MULVN = 3, DIVVN = 4, MODVN = 5,
				ADDNV = 6, SUBNV = 7, MULNV = 8, DIVNV = 9, MODNV = 10,
				ADDVV = 11, SUBVV = 12, MULVV = 13, DIVVV = 14, MODVV = 15,
				SETNUM = 16, SETUN = 17, SETNULL = 18, SETBOOL = 19,
				COPYR = 20, COPYV = 21,
				MAKEST = 30, CALL = 31, FNEW = 32,
				EXIT = 40, RET = 41, RETEXP = 42
			};


			enum class UnlinkedType : intptr_t {
				UNDECLARED = 0, UNDEFINED = 1, NULL_NULL = 2, TRUE = 3, FALSE = 4
			};

			enum class LinkedType : char {
				JSNUMBER = 2, JSSTRING = 3, JSOBJECT = 4, JSARRAY = 5,
				STACK = 6, DATA_PAGE = 7, VARSLOT = 8
			};

			struct Token {
				TokenType type = TokenType::NONE;
				KeyWord word = KeyWord::UNKNOWN;
				Punctuator punc = Punctuator::NONE;
				int lineNumber = 1;
				bool boolValue = false;
				std::string literalValue;
			};
		}
	}
}

#endif //WEBR_CONST

