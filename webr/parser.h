#ifndef WEBR_PARSER
#define WEBR_PARSER

#include <string>
#include <deque>
#include <vector>
#include <map>
#include "const.h"
//#include "lexar.h"
#include "node.h"
#include "engine.h"
namespace webr{
	class Parser;
	class JSValue;
	class JSNumber;
	class JSString;
	class JSBool;
	class Engine;
	class JSReference;
	class JSContext;
	class JSArgs;
	class IdentifierExpr;
	class Routine;
	struct Token;
	using namespace Internal::Enumerations;
	class Expression : public Node {
	public:
		Expression(int type);
		virtual ~Expression();
		virtual JSValue* getValue(Engine *e, Routine *rout) = 0;

		bool IsIdentifier();
		bool IsModifier();
		IdentifierExpr* ToIdentifier();


		int ExprType();
	private:
		int exprtype;
	};
	/*
	class NumberExpr : public Expression {
		double val;
	public:
		NumberExpr(double _val) : Node(NodeType::NUMBER_LITERIAL), val(_val) {};
		JSValue* getValue(Engine *e);

		int Type();
	};
*/

	class Block : public Node {
	public:
		Block(std::vector<Node*> _nodes);

		JSValue* getValue(Engine *e, Routine *owner);

	private:
		std::vector<Node*> nodes;
		JSContext *cx;
	};

	class Assignment : public Expression {
	public:
		Assignment(std::vector<std::vector<Node*>> _refs, bool isVar);

		JSValue* getValue(Engine *e, Routine *rout);
		void setValue(Expression *assignee, JSValue *value);
	private:
		Engine *e;
		Routine *owner;
		std::vector<std::vector<Node*>> refs;
		bool isVar;
	};

	class While : public Node {
	public:
		While(Node *control, Node *block);

		JSValue* getValue(Engine *e, Routine *rout);

	private:
		Node *control;
		Node *block;
	};

	class ForkExpr : public Node {
	public:
		ForkExpr(Node* _exp);
		JSValue *getValue(Engine *e, Routine *rout);
	private:
		Node *exp;
	};

	class IfStatement : public Node {
	public:
		IfStatement(std::vector<Node*> _controls, std::vector<Node*> _blocks, Node* _final_else);

		JSValue* getValue(Engine *e, Routine *rout);

	private:
		std::vector<Node*> controls;
		std::vector<Node*> blocks;
		Node *final_else;
	};


	class IdentifierExpr : public Expression {
	public:
		IdentifierExpr(std::string name);

		std::string getName();
		JSValue* getValue(Engine *e, Routine *rout);
		JSReference*  getReference(Engine *e);
	private:
		Engine *e;
		std::string name;
	};
	class JSBinaryExpr : public Expression {
	public:
		JSBinaryExpr(std::deque<Node*> nodes);

		~JSBinaryExpr();

		JSValue* getValue(Engine *e, Routine *rout);
		int Type();
		int BaseType();

	private:
		std::vector<char> *buffer;
		std::deque<Node*> nodes;
		Engine *e;

		JSValue* UnaryEval(int op, JSValue *v);
		JSValue* Eval(int op, JSValue *lhs, JSValue *rhs);
		JSValue* Add(JSValue* lhs, JSValue *rhs);
		JSValue* Subtract(JSValue *lhs, JSValue *rhs);
		JSValue* Remainder(JSValue *lhs, JSValue *rhs);
		JSValue* Divide(JSValue *lhs, JSValue *rhs);
		JSValue* Multiply(JSValue *lhs, JSValue *rhs);
		JSBool* Equal_To(JSValue *lhs, JSValue *rhs);
		JSBool* Greater_Than(JSValue *lhs, JSValue *rhs);
		JSValue* Less_Than(JSValue *lhs, JSValue *rhs);

		bool IsUnary(int punc);

		bool AnyString(JSValue *a, JSValue *b);

	};

	class ModifierExpr : Expression {
		friend Parser;
	public:
		JSValue* getValue(Engine *e, Routine* owner);

	private:
		ModifierExpr(Node *base);
		struct Modifier {
			int type; //1 = index 2 = memberAccess
			int index;
			std::string memberName;
		};
		Engine *e;
		Node *base;
		void addMemberAccess(std::string name);
		std::vector<Modifier> modifiers;
	};

	class CallExpr : public Expression {
		friend Parser;
		Node *callee;
		std::vector<Node*> args;

	public:
		~CallExpr();

		JSValue* getValue(Engine *e, Routine *rout);
		int Type();
		int BaseType();

	private:
		CallExpr(Node *_callee, std::vector<Node*> _args);
		Engine *e;
		JSArgs* buildArgs(Routine *owner);
		JSValue* calleeObject();
	};

	class Prototype {
	  std::string Name;
	  std::vector<std::string> Args;
	public:
	  Prototype(const std::string &name, const std::vector<std::string> &args)
		: Name(name), Args(args) {}
	};

	class FunctionBody {
	  Prototype *Proto;
	  Node *Body;
	public:
	  FunctionBody(Prototype *proto, Node *body)
		: Proto(proto), Body(body) {}
	};

	class ParserError : public std::exception {
		const char * what() const throw();
	};



	class APunctuator : public Node {
	public:
		APunctuator(int a) : Node(NodeType::APUNCTUATOR), punc(a) {}
		~APunctuator(){}
		int getPunc(){
			return punc;
		}
		JSValue* getValue(Engine *e, Routine *rout){};

	private:
		int punc;
	};

	class Parser{
	public:
		Parser();
		Parser(std::vector<Internal::Enumerations::Token> _tokens);
		std::vector<Node*>& parse();

		void operator()(std::vector<Internal::Enumerations::Token> _tokens);
	private:

		bool InBinOp;
		bool InAssign;
		bool InFuncCall;

		bool lastIsNewLine;
		//Node *lastExpr;

		std::map<int, int> opPrecedence = {
			{(int)Punctuator::OPEN_PARENTHESES, 1},
			{(int)Punctuator::CLOSE_PERENTHESES, 1},
			{(int)Punctuator::MULTIPLY, 2},
			{(int)Punctuator::DIVIDE, 2},
			{(int)Punctuator::REMAINDER, 2},
			{(int)Punctuator::PLUS, 3},
			{(int)Punctuator::MINUS, 3},
			{(int)Punctuator::SHIFT_LEFT, 4},
			{(int)Punctuator::SHIFT_RIGHT, 4},
			{(int)Punctuator::SHIFT_RIGHT_UNSIGNED, 4},
			{(int)Punctuator::LESS_THAN, 5},
			{(int)Punctuator::LT_EQUAL, 5},
			{(int)Punctuator::GREATER_THAN, 5},
			{(int)Punctuator::GT_EQUAL, 5},
			{(int)Punctuator::EQUAL_TO, 6},
			{(int)Punctuator::NOT_EQUAL_TO, 6},
			{(int)Punctuator::PERCISION_EQUAL_TO, 6},
			{(int)Punctuator::PERCISION_NOT_EQUAL_TO, 6},
			{(int)Punctuator::BITWISE_AND, 7},
			{(int)Punctuator::BITWISE_XOR, 8},
			{(int)Punctuator::BITWISE_OR, 9},
			{(int)Punctuator::AND, 10},
			{(int)Punctuator::OR, 11},
		};

		std::vector<Internal::Enumerations::Token> tokens;
		std::vector<Node*> ast;
		size_t pos;
		int currentSlot;

		Internal::Enumerations::Token nextNonNewLine();
		Internal::Enumerations::Token current();
		Internal::Enumerations::Token next();
		Internal::Enumerations::Token peek();
		Internal::Enumerations::Token back(int a);
		Internal::Enumerations::Token backNonNL();
		Internal::Enumerations::Token nextSkipSemicolon();
		Internal::Enumerations::Token peekNonNL(int run);
		void unexpected(Internal::Enumerations::Token to);
		Internal::Enumerations::Token a;

		void expect(int type, int token);

		void Error(std::string err);
		void parseNumberExpr();
		void parseFunction();
		bool IsOperatorLessThen(int, int);
		bool IsBinOperant(Internal::Enumerations::Token to);
		bool IsBinOperant(int to);

		bool isStopper(Internal::Enumerations::Token to);

		int GetWeight(int);

		Node* handle();
		Node* handleFunctionCall(Node *ref);
		Node* handleRhsExpr(Node *rhs);
		Node* handleIdentifier();
		Node* handleExpression(int term, bool inBinOperation);
		Node* handleBinaryExpr(int term, Node * firstNode);
		Node* handleBinaryExpr2(Node * firstNode);
		Node*  handleWord();
		Node* handleFuncArgs();
		Node* handleTerm();
		Node* handleAssignment(bool isVar, Node *n);
		Node* handleWhileST();
		Node* handleBlock();
		Node* handleIfSt();
		Node* handleFork();
		Node* handleObjectLit();
		Node* handleModifier(Node *node);
		bool handleSecondaryExpr(Node *&base, int terminator);


		JSString* WrapString(std::string str);
		JSNumber* WrapNumber(std::string num);
		JSBool* WrapBoolean(bool value);
	};
}
#endif //WEBR_PARSER
