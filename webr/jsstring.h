
#ifndef WEBR_JSSTRING
#define WEBR_JSSTRING

#include "jsvalue.h"
#include <string>
namespace webr{
	class JSString : public JSValue {
	public:
		JSString(std::string _value);
		int length();

		JSValue* getValue(Engine *e, Routine *owner);
		std::string ToStringValue();
		bool ToBooleanValue();

		std::string str();

		void Dispose();
	private:
		std::string *ptr_value;
	};

}
#endif
