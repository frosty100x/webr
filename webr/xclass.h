#ifndef WEBR_XCLASS
#define WEBR_XCLASS

#include "stddef.h"
#include "sys/types.h"
#include <cstdint>
namespace webr {

	typedef int16_t slot;
	typedef intptr_t obp;
	namespace Internal {
		namespace JSObjects {

			namespace {
				//size_t _lastAllocation = 0;

				size_t PAGE_SIZE_OFFSET = 1;
				size_t _PAGE_SIZE = 4;

				size_t PAGE_SPACE_OFFSET = 5;

				char* pageHead;
				char* spaceHead;

				size_t avalible = 0;
				size_t INIT_PAGE_SIZE = 1000000; //1 mbyte
				//---

				static int INIT_NUMBER_OBJECT_SIZE = 5;
				static int INIT_STRING_OBJECT_SIZE = 15;

				static int OBJECT_TYPE_OFFSET = 0;
				static int OBJECT_VALUE_OFFSET = OBJECT_TYPE_OFFSET + 1;
				static int INIT_NUMBER_SIZE = 4; //32bit
			}

			namespace xJSNumber {

				intptr_t make();
				intptr_t make(int32_t num);
				void setValue(intptr_t objectPtr, int32_t value);
				int32_t getValue(intptr_t objectPtr);
			}

			namespace xJSString {

			}

		}
	}
}

#endif //WEBR_XCLASS
