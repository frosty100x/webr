#ifndef WEBR_JSUNDEFINED
#define WEBR_JSUNDEFINED
#include "jsvalue.h"
namespace webr{
	class Engine;
	class JSUndefined : public JSValue {
	public:
		JSUndefined();

		JSValue* getValue(Engine *e, Routine *owner);

		std::string ToStringValue();
		bool ToBooleanValue();
		void Dispose();
	};
}
#endif //WEBR_JSUNDEFINED
