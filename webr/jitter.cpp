#include "const.h"
#include "jitter.h"
#include "huballocator.h"
#include "stack.h"
//#include "xjsfunction.h"
#include "xclass.h"
#include "codegen.h"
#include <iostream>
namespace webr {

	namespace Internal {
		/*
		 * Standard 6 byte instruction
		 *  |op| |A| |B| |C|
		 *  |op| |A| |  D  |
		 */

		using namespace JSObjects;
		using namespace Enumerations;

		Jitter::Jitter() {
			codeBuff = 0;
			currentStack = (obp)Stack::make(); //global stack
			constantTable = Stack::make();
			codePos = 0;

			this->set(1, xJSNumber::make(0));
		}

		void Jitter::Run(char* code) {
			codeBuff = code;
			bool end = false;
			while (!end) {
				auto c = (OpCode)*codeBuff;
				switch (c) {

				case OpCode::ADDVV: addvv(); break;
				case OpCode::ADDVN: addvn(); break;
				case OpCode::ADDNV: addnv(); break;
				case OpCode::SUBVN: subvn(); break;
				case OpCode::SUBNV: subnv(); break;
				case OpCode::SUBVV: subvv(); break;
				case OpCode::SETNUM: setnum(); break;
				case OpCode::MULVN: mulvn(); break;
				case OpCode::MULNV: mulnv(); break;
				case OpCode::MULVV: mulvv(); break;
				case OpCode::DIVVN: divvn(); break;
				case OpCode::DIVNV: divnv(); break;
				case OpCode::DIVVV: divvv(); break;
				case OpCode::COPYR: copyr(); break;
				case OpCode::SETUN: setun(); break;
				case OpCode::SETBOOL: setbool(); break;
				default: /*case OpCodes::EXIT:*/
					end = true;
				}
			}

			std::cout << "Last: " <<
				xJSNumber::getValue(Stack::getSlotPtr((obp)currentStack, 1)) << std::endl;
		}

		/*
		//Set Destination to Literal
		//dest = Destination slot | a = Literal slot
		void Jitter::knum(){

			int16_t dest = nextAs16BitNum();

			intptr_t lit = nextAsSlotPointer();

			Stack::push((intptr_t)currentStack, dest,
					xJSNumber::make()
			);

		}
		*/


		void Jitter::setbool() {
			codeBuff++;

			int16_t dest = nextAs16BitNum();
			codeBuff += 2;

			char value = nextAs8BitNum();
			codeBuff += 1;

			Stack::push(currentStack, dest, value);
		}

		//dest = slot, value = int value to store in slot
		void Jitter::setnum() {
			codeBuff++;

			int16_t dest = nextAs16BitNum();
			codeBuff += 2;

			int16_t value = nextAs16BitNum();
			codeBuff += 2;

			intptr_t s = xJSNumber::make(value);
			Stack::push(currentStack, dest, s);

			//std::cout << xJSNumber::getValue(Stack::getSlotPtr((intptr_t)currentStack, dest)) << std::endl;
		}


		//Set a slot to undefined //0
		void Jitter::setun() {
			codeBuff++;

			slot source = nextAs16BitNum();
			codeBuff += sizeof(slot);

			Stack::push(currentStack, source, 0);
		}

		//Copy Slot Reference
		//dest = place to copy object pointer to
		//source = place to copy object from
		void Jitter::copyr() {
			codeBuff++;

			slot dest = nextAs16BitNum();
			codeBuff += sizeof(slot);

			obp source = nextAsObjectPointer();
			codeBuff += sizeof(slot);

			Stack::push(currentStack, dest, source);
		}

		intptr_t Jitter::DoOp(char op, int16_t v1, int16_t v2) {

			switch (op) {
			case '+':
				return (obp)new int16_t(v1 + v2);
			case '-':
				return (obp)new int16_t(v1 - v2);
			case '*':
				return (obp)new int16_t(v1*v2);
			case '/':
				return (obp)new int16_t(v1 / v2);
			}
			return NULL;
		}

		//dest = slot, a = slot, b = literal
		void Jitter::OpVN(char op) {
			codeBuff++;

			slot dest = nextAs16BitNum();
			codeBuff += 2;

			obp a = nextAsObjectPointer();
			codeBuff += 2;

			slot b = nextAs16BitNum();
			codeBuff += 2;

			Stack::push((intptr_t)currentStack, dest,
				xJSNumber::make((*(int16_t*)DoOp(op, xJSNumber::getValue(a), b)))
				);
		}

		void Jitter::OpNV(char op) {
			codeBuff++;

			slot dest = nextAs16BitNum();
			codeBuff += 2;

			slot a = nextAs16BitNum();
			codeBuff += 2;

			obp b = nextAsObjectPointer();
			codeBuff += 2;

			Stack::push((intptr_t)currentStack, dest,
				xJSNumber::make((*(int16_t*)DoOp(op, xJSNumber::getValue(b), a)))
				);

		}

		// dest = RESULT DESTINATION SLOT | A = VAR1 |B = VAR2
		void Jitter::OpVV(char op) {
			codeBuff++;

			slot dest = nextAs16BitNum();
			codeBuff += 2;

			obp a = nextAsObjectPointer();
			codeBuff += 2;

			obp b = nextAsObjectPointer();
			codeBuff += 2;

			Stack::push((obp)currentStack, dest,
				xJSNumber::make((*(slot*)DoOp(op,
					xJSNumber::getValue(a), xJSNumber::getValue(b)
					)))
				);
		}

		inline void Jitter::addvn() { OpVN('+'); }

		inline void Jitter::addnv() { OpNV('+'); }

		inline void Jitter::addvv() { OpVV('+'); }

		inline void Jitter::subvn() { OpVN('-'); }

		inline void Jitter::subvv() { OpVV('-'); }

		inline void Jitter::subnv() { OpNV('-'); }

		inline void Jitter::mulvn() { OpVN('*'); }

		inline void Jitter::mulvv() { OpVV('*'); }

		inline void Jitter::mulnv() { OpNV('*'); }

		inline void Jitter::divvn() { OpVN('/'); }

		inline void Jitter::divnv() { OpNV('/'); }

		inline void Jitter::divvv() { OpVV('/'); }

		//void Jitter::call(){

			//char* funcPtr = (char*)nextAsSlotPointer();

			//char *block = JSObjects::xJFunction::callableBlock(funcPtr);



			//Stack stack = new Stack();
		//}

		//void Jitter::expect(unsigned char i){
		//	if(i >= codePos - codeSize)
		//		throw "IR ERROR";
		//}

		inline slot Jitter::byte2To16Bit(unsigned char a, unsigned char b) {
			return (slot)b + (a << 8);
		}

		inline unsigned char Jitter::nextByte() {
			return (unsigned char)codeBuff[codePos++];
		}

		inline intptr_t Jitter::nextAsObjectPointer() {
			//return Stack::getSlot((intptr_t)currentStack, byte2To16Bit(nextByte(), nextByte()));
			return Stack::getSlotPtr(currentStack, ((slot*)codeBuff)[0]);
		}

		inline slot Jitter::nextAs16BitNum() {
			return ((slot*)codeBuff)[0];
		}

		inline char Jitter::nextAs8BitNum() {
			return ((char)codeBuff[0]);
		}

		inline void Jitter::set(slot slot, obp objPtr) {
			Stack::push(currentStack, slot, objPtr);
		}

		inline void Jitter::changeStack(void *p) {
			//stackPointer = p;
		}
	}
}
