
#include "parser.h"
#include "jsundefined.h"
#include "engine.h"
#include "jsreference.h"
#include "node.h"
#include "jsvalue.h"
#include "jscontext.h"
#include "errortypes.h"
#include "routine.h"
namespace webr {


	Assignment::Assignment(std::vector<std::vector<Node*>> _refs, bool isVar)
		: Expression(ExpressionType::ASSIGNMENT){
		refs = _refs;
		this->isVar = isVar;
	}

	/*
	void Assignment::setValue(std::string name, JSValue *value){
		if(isVar) e->Context()->Set(name, value);
		else e->Context()->GlobalSet(name, value);
	}
	*/
	void Assignment::setValue(Expression *assignee, JSValue *value){

		if(assignee->IsIdentifier()){
			if(isVar) e->Context()->Set(assignee->ToIdentifier()->getName(), value);
			else e->Context()->GlobalSet(assignee->ToIdentifier()->getName(), value);
		}else if(assignee->IsModifier()){
			//assignee->getValue(e, owner)->
		}


	}


	JSValue* Assignment::getValue(Engine *e, Routine *owner){
		this->e = e;
		this->owner = owner;
		JSValue *toReturn = NULL;
		for(size_t i=0; i < refs.size(); i++){

			if(refs.at(i).size() < 2){
				Node *list = refs.at(i).at(0);
				//Theoretically this should never be possible as first item should always be an Identifier.
				//if(!(n->IsExpression() && n->ToExpression()->IsIdentifier()))
				  //throw ReferenceError
				setValue(list->ToExpression(), e->Undefined());
			}else{
				std::vector<Node*> &slist = refs.at(i);
				Node *last = slist.back();
				JSValue *toAssign = nullptr;

				for(size_t n=0; n < slist.size()-1; n++){
					Node *a = slist.at(n);

					if(a->IsJSValue())
						throw ReferenceError("Invalid left-hand side in assignment.", 1);
					//if(!(a->IsExpression() && a->ToExpression()->IsIdentifier()))
						//throw ReferenceError(a->ToExpression()->ToIdentifier()->getName() + " is not defined", 1);


					toAssign = last->getValue(e, owner);
					setValue(a->ToExpression(), toAssign);
					if(!isVar)
						toReturn = toAssign;
				}
			}
			//owner->holt();
		}
		return (toReturn == NULL) ? e->Undefined() : toReturn;
	}


}
