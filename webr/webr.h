/*
 * Author: Greg Batie
 * Webr Engine Main header.
 *
 */

//class Engine;
//class Node;
//class JSValue;
//class Parser;
//class Lexar;
//class Expr;

#include <iostream>
#include <stdlib.h>
#include <cstdlib>
#include <typeinfo>
#include <memory>
#include <string>
#include <vector>
#include "engine.h"
#include "node.h"
#include "const.h"
#include "lexar.h"
#include "parser.h"
#include "jsvalue.h"
#include "jsstring.h"
#include "jsnumber.h"
#include "jsfunction.h"
#include "jsbool.h"
#include "jsundefined.h"

#define FUNC(en, func, name) en->Context()->Set(name, en->NewFunction(func, name));

#define STR(en, name, value) en->Context()->Set(name, en->NewString(value));

#define NUM(en, name, value) en->Context()->Set(name, en->NewNumber(value));
