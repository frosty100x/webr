#ifndef WEBR_JSCONTEXT
#define WEBR_JSCONTEXT

#include <vector>
#include <string>
#include <deque>
namespace webr {
	class Engine;
	class JSReference;
	class JSValue;
	class JSContext {
	public:
		~JSContext();
		JSContext(Engine *e);
		JSContext(Engine *e, JSContext *_parent);
		JSReference* Get(std::string name);
		void Set(std::string name, JSValue *value);

		JSContext(Engine *e, JSContext *_parent, JSContext *_global);
		Engine* GetEngine();
		void GlobalSet(std::string name, JSValue *value);
	private:

		bool IsGlobal();
		JSContext* globalCx();

		JSReference* localExist(std::string name);
		JSReference* globalExist(std::string name);
		JSReference* exist(std::string name);
		std::deque<JSReference*> refs;
		//std::map<std::string, JSValue*> refs;
		JSContext *parent;
		JSContext *global;
		Engine *e;
	};

}
#endif //WEBR_CONTEXT
