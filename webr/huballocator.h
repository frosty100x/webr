#ifndef WEBR_ALLOCATOR
#define WEBR_ALLOCATOR

#include "stddef.h"
#include <cstdint>
#include "limits.h"
namespace webr {
	namespace Internal {
		namespace HubAllocator {
			namespace {

				size_t _lastAllocation = 0;

				size_t PAGE_SIZE_OFFSET = 1;
				size_t _PAGE_SIZE = 4;

				size_t PAGE_SPACE_OFFSET = 5;

				char* pageHead;
				char* spaceHead;


				size_t avalible = 0;
				size_t INIT_PAGE_SIZE = 1000000; //1 mbyte

				void format(char* page, int32_t size);
				void* makeSpace(int mode);
			}


			void* getSpace(size_t size);

			void* getExecutableSpace(size_t size);

			//char* checkPage();


			//void* makeSpace(int size, int mode);

		}
	}
}

#endif //WEBR_ALLOCATOR
