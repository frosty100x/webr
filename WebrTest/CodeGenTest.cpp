#include "stdafx.h"
#include "CppUnitTest.h"

#include "../webr/codegen.h"
#include "../webr/xparser.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace webr::Internal;

namespace WebrTest
{		
	TEST_CLASS(CodeGenTest)
	{
	public:

		CodeBuff *buf;
		#define Parse(x) buf = ((new xParser(20))->parse(x));
		
		TEST_METHOD(TestNumberDeclaration)
		{
			Parse("var n = 8;");
			Assert::AreEqual(5, buf->codeLength);

			Parse("var n = 99;");
			Assert::AreEqual(5, buf->codeLength);
		}

		TEST_METHOD(TestStringDeclaration)
		{
			Parse("var n = 'This is a test';");
			Assert::AreEqual(17, buf->codeLength);
		}

	};
}