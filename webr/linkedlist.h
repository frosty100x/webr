
namespace webr {
	namespace util {

		struct _linked_item {
			void * data;
			_linked_item * next;
		};

		struct _iterator {
			_linked_item * current;
			_linked_item * operator++() {
				if(current->next != nullptr)
					return current = current->next;
				return 0;
			}

			_iterator* operator()(_linked_item *it){
				this->current = it;
				return this;
			}
		};

		template<class T> class LinkedList {
		public:


			LinkedList(int);

			void push_back(T el);
			int size();

			_iterator* begin();
			T front();
			T back();

		private:
			int _size;
			_linked_item *head;
			//T *current;
			_linked_item *tail;
		};
	}
}
