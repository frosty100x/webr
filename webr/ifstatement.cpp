#include "parser.h"
#include "engine.h"
#include "jsundefined.h"
namespace webr {

	IfStatement::IfStatement(std::vector<Node*> _controls, std::vector<Node*> _blocks, Node* _final_else)
		: Node(NodeType::IF_STATEMENT) {
		controls = _controls;
		blocks = _blocks;
		final_else = _final_else;
	}

	JSValue* IfStatement::getValue(Engine *e, Routine *owner){
		bool enter = false;
		JSValue *last = nullptr;

		for(size_t i=0; i< controls.size(); i++){
			enter = controls.at(i)->getValue(e, owner)->ToBooleanValue();


			if(enter){
				//TODO: Fix! this dosen't necessarily tell us if the current condition has a block!
				if(blocks.size() > i)
					last = blocks.at(i)->getValue(e, owner); break;
			}
		}
		if(!enter && final_else != NULL)
			last = final_else->getValue(e, owner);

		return last;
	}


}
