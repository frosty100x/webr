
#ifndef WEBR_NODE
#define WEBR_NODE
#include "engine.h"
//#include <boost/coroutine/all.hpp>
//#include "jsvalue.h"
namespace webr {
	class JSValue;
	class Engine;
	class Expression;
	class APunctuator;
	class JSReference;
	class Routine;
	class Node {
	public:
		Node(int type);
		virtual ~Node() = 0;
		int NodeType();

		bool IsJSValue();
		bool IsExpression();
		bool IsAPunctuator();
		bool IsReference();

		JSValue* ToJSValue();
		Expression* ToExpression();
		APunctuator* ToAPunctuator();
		JSReference* ToReference();

		//void testMethod(boost::coroutines::coroutine<JSValue*>::pull_type &sink);

		virtual JSValue* getValue(Engine *e, Routine *owner) = 0;
	private:
		int nodetype;
	};
}
#endif //WEBR_NODE
