#include <string>
#include "errortypes.h"
#include "util.h"

namespace webr {

	ReferenceError::ReferenceError(std::string _mes, int _lineNumber) : lineNumber(_lineNumber), mes(_mes) {

	}

	std::string ReferenceError::message(){
		return "Reference Error: " + mes + " at line " + util::ntos(lineNumber);
	}


}
