
#include "errortypes.h"
#include "util.h"
#include "const.h"

namespace webr{
	using namespace Internal::Enumerations;
	SyntaxError::SyntaxError(std::string _mes, Token _to) : mes(_mes), to(_to) {}

	SyntaxError::SyntaxError(std::string _mes) : mes(_mes) {}

	std::string SyntaxError::message(){
		return "Syntax Error: "+ mes + value(to) + " at line " + util::ntos(to.lineNumber);
	}

	std::string SyntaxError::value(Token to){
		switch(to.type){
		case TokenType::PUNCUATOR:
			return FromPunctuator(to.punc);
		case TokenType::KEYWORD:
			return FromKeyWord(to.word);
		case TokenType::NUMBER_LITERAL: case TokenType::STRING_LITERAL: case TokenType::IDENTIFIER:
			return to.literalValue;
		case TokenType::EOF_EOF:
			return "EOF";
		case TokenType::UNDEFINED_LITERAL:
			return "undefined";
		default: return "unknown";
		}
	}

}
